FROM php:7.2-apache-buster

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# install deps
RUN apt update -y \
    && apt install -y git wget unzip postgresql-client libcurl4-openssl-dev libpq-dev libpng-dev gpg libjpeg-dev locales libxslt-dev acl

# php extensions
RUN docker-php-ext-configure gd --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) curl gd pdo_pgsql opcache xsl intl zip

# locale
RUN echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen && locale-gen
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
ENV LC_ALL fr_FR.UTF-8

# apache config
ENV APACHE_DOCUMENT_ROOT=/var/www/html/web
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
# RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite

# php config
RUN mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini \
    && sed -i 's/max_execution_time = .*/max_execution_time = 300/' $PHP_INI_DIR/php.ini \
    && sed -i 's/max_input_time = .*/max_input_time = 60/' $PHP_INI_DIR/php.ini \
    && sed -i 's/memory_limit = .*/memory_limit = 128M/' $PHP_INI_DIR/php.ini \
    && sed -i 's/post_max_size = .*/post_max_size = 64M/' $PHP_INI_DIR/php.ini \
    && sed -i 's/upload_max_filesize = .*/upload_max_filesize = 64M/' $PHP_INI_DIR/php.ini \
    && sed -i 's/max_file_uploads = .*/max_file_uploads = 20/' $PHP_INI_DIR/php.ini \
    && sed -i 's/max_input_vars = .*/max_input_vars = 3000/' $PHP_INI_DIR/php.ini \
    && sed -i 's/variables_order = .*/variables_order = EGPCS/' $PHP_INI_DIR/php.ini

# install composer
COPY --from=composer:1.7 /usr/bin/composer /usr/bin/composer

# install node 9 & yarn manager
RUN curl -sL https://deb.nodesource.com/setup_9.x | bash - \
    && apt-get install nodejs=9.11.2-1nodesource1 -y \
    && npm install -g yarn gulp

# set workdir
WORKDIR /var/www/html

# install dependencies
COPY composer.* ./
ADD app/AppKernel.php app/
ADD app/AppCache.php app/
ADD common/datatourisme/webapp-bundle common/datatourisme/webapp-bundle
RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-scripts \
    --prefer-dist

# # we cannot do that :(
# COPY package*.json yarn.lock ./
# RUN yarn

# copy source
COPY . .
RUN yarn install

# composer postscript
RUN mkdir var
RUN composer run-script --timeout=0 post-install-cmd

# RUN gulp
RUN gulp

# set default permissions for /var/www/html/var
RUN setfacl -dR -m u:www-data:rwX var
RUN setfacl -R -m u:www-data:rwX var