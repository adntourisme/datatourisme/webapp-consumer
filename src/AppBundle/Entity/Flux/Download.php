<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Flux;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Application\Application;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Class Download.
 *
 * @ORM\Table(name="download", indexes={
 *  @ORM\Index(name="created_at", columns={"created_at"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DownloadRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Download
{
    use ORMBehaviors\Timestampable\Timestampable;

    const DOWNLOAD_MANUAL = 'manual';
    const DOWNLOAD_WEBSERVICE = 'webservice';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Application
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application\Application", inversedBy="downloads")
     * @ORM\JoinColumn(onDelete="CASCADE", nullable=true)
     */
    protected $application;

    /**
     * @var string
     * @ORM\Column(type="string", length=16)
     */
    protected $type;

    /**
     * @var Flux
     * @ORM\ManyToOne(targetEntity="Flux", inversedBy="downloads", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $flux;

    /**
     * @var OutputType
     * @ORM\ManyToOne(targetEntity="OutputType")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $outputType;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $nbrPOI;

    /**
     * @var int
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected $size;

    /**
     * Download constructor.
     *
     * @param Process $process
     */
    public function __construct(Process $process)
    {
        $this->flux = $process->getFlux();
        $this->setOutputType($process->getOutputType());
        $this->setNbrPOI($process->getNbrPOI());
        $this->setSize($process->getSize());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param Application $application
     */
    public function setApplication($application)
    {
        $this->application = $application;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return Flux
     */
    public function getFlux()
    {
        return $this->flux;
    }

    /**
     * @param Flux $flux
     */
    public function setFlux($flux)
    {
        $this->flux = $flux;
    }

    /**
     * @return OutputType
     */
    public function getOutputType()
    {
        return $this->outputType;
    }

    /**
     * @param OutputType $outputType
     */
    public function setOutputType($outputType)
    {
        $this->outputType = $outputType;
    }

    /**
     * @return int
     */
    public function getNbrPOI()
    {
        return $this->nbrPOI;
    }

    /**
     * @param int $nbrPOI
     */
    public function setNbrPOI($nbrPOI)
    {
        $this->nbrPOI = $nbrPOI;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->getFlux()->updateUsedAt();
        $this->getFlux()->setLastDownload($this);
        $this->getFlux()->setLastDownloadAt(time());
    }
}
