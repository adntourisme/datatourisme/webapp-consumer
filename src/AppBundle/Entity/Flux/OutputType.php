<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Flux;

use AppBundle\Entity\AbstractType;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="output_type")
 * @ORM\Entity()
 */
class OutputType extends AbstractType
{
    const TYPE_SELECT = 'select';
    const TYPE_CONSTRUCT = 'construct';

    /**
     * @var string
     * @ORM\Column(name="type", type="string", length=16, nullable=true)
     */
    protected $type;

    /**
     * @var string
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    protected $requestType;

    /**
     * @var string
     * @ORM\Column(type="string", length=16)
     */
    protected $extension;

    /**
     * @var string
     * @ORM\Column(type="string", length=32)
     */
    protected $mime;

    /**
     * @var string
     * @ORM\Column(type="string", length=16)
     */
    protected $code;

    /**
     * @var bool
     * @ORM\Column(name="_default", type="boolean")
     */
    protected $default = false;

    /**
     * @var bool
     * @ORM\Column(name="localized", type="boolean")
     */
    protected $localized = true;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getRequestType()
    {
        return $this->requestType;
    }

    /**
     * @param string $requestType
     */
    public function setRequestType($requestType)
    {
        $this->requestType = $requestType;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @param string $mime
     */
    public function setMime($mime)
    {
        $this->mime = $mime;
    }

    /**
     * @return bool
     */
    public function isDefault()
    {
        return $this->default;
    }

    /**
     * @return bool
     */
    public function isLocalized()
    {
        return $this->localized;
    }

    /**
     * Get a Query type from SPARQL query.
     *
     * @param $query
     *
     * @return string
     */
    public static function getTypeFromQuery($query)
    {
        $query = strtolower($query);

        $types = array(
            self::TYPE_SELECT => false,
            self::TYPE_CONSTRUCT => false,
        );

        // compute all positions
        foreach ($types as $type => $val) {
            $pos = strpos($query, strtolower($type));
            $types[$type] = $pos;
        }

        // find smallest position to determine the first keyword
        $requestType = null;
        $pos = false;
        foreach ($types as $type => $val) {
            if (false !== $val) {
                if (false !== $val && (false === $pos || $val < $pos)) {
                    $requestType = $type;
                    $pos = $val;
                }
            }
        }

        return $requestType;
    }
}
