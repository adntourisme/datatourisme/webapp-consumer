<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Entity\Flux;

use AppBundle\Entity\User\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Flux.
 *
 * @ORM\Table(name="flux")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FluxRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Flux
{
    use DataGouvTrait;
    use ORMBehaviors\Timestampable\Timestampable;
    use ORMBehaviors\Sluggable\Sluggable;

    // Despite the fact it should not happen, adding modes would require adaptations in FluxVoter and EditorController
    const MODE_VISUAL = 'visual';
    const MODE_EXPERT = 'expert';

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=31, nullable=false)
     */
    private $mode;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User\User", inversedBy="flux")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"Default", "create"})
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotBlank(groups={"Default", "create"})
     */
    protected $description;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $query;

    /**
     * @var object
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $queryData;

    /**
     * @var object
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    protected $queryType;

    /**
     * @var OutputType
     * @ORM\ManyToOne(targetEntity="OutputType")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    protected $outputType;

    /**
     * @var array
     * @ORM\Column(type="simple_array", nullable=false)
     * @Assert\NotBlank(message="empty_languages")
     */
    protected $languages;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    protected $apiId;

    /**
     * @var ArrayCollection|Process[]
     * @ORM\OneToMany(targetEntity="Process", mappedBy="flux")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $processes;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\RdfResource", mappedBy="flux")
     */
    private $rdfResources;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $scheduleHour;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $executedAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $usedAt;

    /**
     * @var int
     * @ORM\Column(type="integer",options={"default" : 0})
     */
    protected $lastProcessAt;

    /**
     * @var int
     * @ORM\Column(type="integer",options={"default" : 0})
     */
    protected $lastDownloadAt;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Download", mappedBy="flux")
     */
    private $downloads;

    /**
     * @var Download
     * @ORM\OneToOne(targetEntity="Download")
     * @ORM\JoinColumn(name="last_download_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $lastDownload;

    /**
     * Flux constructor.
     *
     * @param User|null $user
     */
    public function __construct($user = null)
    {
        $this->user = $user;
        $this->setMode(self::MODE_VISUAL);
        $this->lastDownloadAt = 0;
        $this->lastProcessAt = 0;
        $this->languages = [];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode(string $mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return object
     */
    public function getQueryData()
    {
        return $this->queryData;
    }

    /**
     * @param object $queryData
     */
    public function setQueryData($queryData)
    {
        $this->queryData = $queryData;
    }

    /**
     * @return OutputType
     */
    public function getOutputType()
    {
        return $this->outputType;
    }

    /**
     * @param OutputType $outputType
     */
    public function setOutputType($outputType)
    {
        $this->outputType = $outputType;
    }

    /**
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param array $languages
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
    }

    /**
     * @return OutputType
     */
    public function getQueryType()
    {
        return $this->queryType;
    }

    /**
     * @param string $queryType
     */
    public function setQueryType($queryType)
    {
        $this->queryType = $queryType;
    }

    /**
     * @return string
     */
    public function getApiId()
    {
        return $this->apiId;
    }

    /**
     * @param string $apiId
     */
    public function setApiId($apiId)
    {
        $this->apiId = $apiId;
    }

    /**
     * @return array
     */
    public function getSluggableFields()
    {
        return array('name');
    }

    /**
     * @ORM\PreFlush()
     */
    public function preFlush()
    {
        $this->apiId = md5($this->getUser()->getId().'/'.$this->getId().'/'.$this->getCreatedAt()->getTimestamp());
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return ArrayCollection
     */
    public function getRdfResources()
    {
        return $this->rdfResources;
    }

    /**
     * @param ArrayCollection $rdfResources
     */
    public function setRdfResources($rdfResources)
    {
        $this->rdfResources = $rdfResources;
    }

    /**
     * @return ArrayCollection
     */
    public function getProcesses()
    {
        return $this->processes;
    }

    /**
     * @param ArrayCollection $processes
     */
    public function setProcesses($processes)
    {
        $this->processes = $processes;
    }

    /**
     * @return Process
     */
    public function getLastProcess($type)
    {
        foreach ($this->processes as $process) {
            if ($process->getType() == $type) {
                return $process;
            }
        }

        return null;
    }

    /**
     * @return Process
     */
    public function getLastSuccessProcess($type)
    {
        foreach ($this->processes as $process) {
            if ($process->getType() == $type && Process::STATUS_OK == $process->getStatus()) {
                return $process;
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getScheduleHour()
    {
        return $this->scheduleHour;
    }

    /**
     * @param string $scheduleHour
     */
    public function setScheduleHour($scheduleHour)
    {
        $this->scheduleHour = $scheduleHour;
    }

    /**
     * @return \DateTime
     */
    public function getExecutedAt()
    {
        return $this->executedAt;
    }

    /**
     * @param \DateTime $executedAt
     */
    public function setExecutedAt($executedAt)
    {
        $this->executedAt = $executedAt;
    }

    /**
     * @return ArrayCollection
     */
    public function getDownloads()
    {
        return $this->downloads;
    }

    /**
     * @param ArrayCollection $downloads
     */
    public function setDownloads($downloads)
    {
        $this->downloads = $downloads;
    }

    /**
     * @return Download
     */
    public function getLastDownload()
    {
        return $this->lastDownload;
    }

    /**
     * @param Download $downloads
     */
    public function setLastDownload($download)
    {
        $this->lastDownload = $download;
    }

    /**
     * @return \DateTime
     */
    public function getUsedAt()
    {
        return $this->usedAt;
    }

    /**
     * @param \DateTime $usedAt
     */
    public function setUsedAt($usedAt)
    {
        $this->usedAt = $usedAt;
    }

    /**
     * Update used at.
     */
    public function updateUsedAt()
    {
        $this->usedAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getChecksum()
    {
        return md5(json_encode([$this->getQuery(), $this->getLanguages(), $this->getOutputType() ? $this->getOutputType()->getCode() : null]));
    }

    /**
     * @return bool
     */
    public function isRunnable()
    {
        return $this->getQuery() && null !== $this->getScheduleHour();
    }

    /**
     * @return int
     */
    public function getLastProcessAt()
    {
        return $this->lastProcessAt;
    }

    /**
     * @param int $lastProcessAt
     */
    public function setLastProcessAt($lastProcessAt)
    {
        $this->lastProcessAt = $lastProcessAt;
    }

    /**
     * @return int
     */
    public function getLastDownloadAt()
    {
        return $this->lastDownloadAt;
    }

    /**
     * @param int $lastDownloadAt
     */
    public function setLastDownloadAt($lastDownloadAt)
    {
        $this->lastDownloadAt = $lastDownloadAt;
    }

    /**
     * @Assert\Callback
     */
    public function constraintCallback(ExecutionContextInterface $context)
    {
        if ($this->isDataGouv() && !$this->getDataSet()) {
            $context->buildViolation('empty_dataset')->atPath('dataSet')->addViolation();
        }
    }
}
