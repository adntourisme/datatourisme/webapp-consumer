<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\Process;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProcessRepository.
 */
class ProcessRepository extends EntityRepository
{
    /**
     * @param Flux|null $flux
     *
     * @return QueryBuilder
     */
    public function createPaginationQueryBuilder(Flux $flux = null, $type): QueryBuilder
    {
        $qb = $this->createQueryBuilder('process')
            ->leftJoin('process.flux', 'flux')
            ->where('flux.id = :flux_id')
            ->andWhere('process.type = :type')
            ->andWhere('process.status NOT IN (:status)')
            ->setParameter('flux_id', $flux->getId())
            ->setParameter('status', [Process::STATUS_WAIT, Process::STATUS_RUNNING])
            ->setParameter('type', $type);

        return $qb;
    }

    /**
     * Clean old entries from the database
     * Keep $max entries for the given flux.
     *
     * @param Flux   $flux
     * @param int    $max
     * @param string $type
     *
     * @return QueryBuilder
     */
    public function cleanOldEntriesQueryBuilder(Flux $flux, $max, $type)
    {
        $parameters = [
            'flux' => $flux,
            'type' => $type,
        ];

        $keep = $this->createQueryBuilder('p');
        $keep
            ->select('p.id')
            ->where('p.flux = :flux')
            ->andWhere('p.type = :type')
            ->orderBy('p.createdAt', 'DESC')
            ->setParameters($parameters)
            ->setMaxResults($max);
        $result = $keep->getQuery()->getScalarResult();
        $ids = array_column($result, 'id');

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete(Process::class, 'p2')
            ->where('p2.flux = :flux')
            ->andWhere('p2.type = :type')
            ->andWhere('p2 <> :process')
            ->andWhere(
                $qb->expr()->notIn('p2.id', $ids)
            )
            ->setParameters($parameters + [
                'process' => $flux->getLastSuccessProcess($type),
            ]);

        return $qb;
    }
}
