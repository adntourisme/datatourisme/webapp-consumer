<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Ontology;

use AppBundle\JavaWorker\JavaWorkerClient;

/**
 * OntologyFactory
 */
class OntologyFactory
{
    /**
     * @var 
     */
    private $javaWorkerClient;

    /**
     * OntologyFactory constructor.
     *
     * @param $javaWorkerClient
     */
    public function __construct(JavaWorkerClient $javaWorkerClient)
    {
        $this->javaWorkerClient = $javaWorkerClient;
    }

    /**
     * Create an ontology instance
     */
    public function create($profile = null) {
        $data = $this->javaWorkerClient->ontology($profile);
        return new Ontology($data);
    }
}
