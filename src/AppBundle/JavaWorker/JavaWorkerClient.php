<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\JavaWorker;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\Process;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;

class JavaWorkerClient
{
    private $client;
    private $scheme = 'http';
    private $host = 'localhost';
    private $port = 8042;
    private $locale = 'fr';

    /**
     * JavaWorkerClient constructor.
     *
     * @param $host
     * @param $port
     * @param RequestStack $requestStack
     */
    public function __construct($host, $port, RequestStack $requestStack)
    {
        $this->host = $host;
        $this->port = $port;
        $this->client = new Client([
            'base_uri' => $this->scheme.'://'.$this->host.':'.$this->port,
            //'timeout'  => 2.0
        ]);
        if ($requestStack->getCurrentRequest()) {
            $this->locale = $requestStack->getCurrentRequest()->getLocale();
        }
    }

    /**
     * Get the ontology json.
     *
     * @param null $profile
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function ontology($profile = null)
    {
        $url = 'ontology'.($profile ? '/'.$profile : '');
        $response = $this->client->get($url.'?lang='.$this->locale);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function sparqlPreview($query)
    {
        $url = 'consumer/sparql/preview';
        $response = $this->client->post($url, ['body' => $query]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param Flux $flux
     * @param $type
     *
     * @return StreamedResponse
     */
    public function download(Flux $flux, $type)
    {
        $response = new StreamedResponse();

        $response->setCallback(function () use ($flux, $type) {
            $url = sprintf('consumer/%d/download/%s', $flux->getId(), $type);
            $resp = $this->client->request('GET', $url, [
                'stream' => true,
                'on_headers' => function (ResponseInterface $resp) use ($flux, $type) {
                    // set some headers
                    header('Content-Type: '.$resp->getHeaderLine('Content-Type'));
                    // set content disposition
                    $disposition = $resp->getHeaderLine('Content-Disposition');
                    preg_match_all('/.*filename=[\\\'\"]?([^\\\'\"]+)/', $disposition, $output);
                    $filename = $output[1][0];
                    $extension = pathinfo($filename)['extension'];
                    $lastModified = strtotime($resp->getHeaderLine('Last-Modified'));
                    $newFilename = sprintf('flux-%s-%s%s.%s', $flux->getId(), date('YmdHi', $lastModified), Process::TYPE_PARTIAL == $type ? '.partial' : '', $extension);
                    header('Content-Disposition: '.str_replace($filename, $newFilename, $disposition));
//                    $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $newFilename);
//                    header("Content-Disposition: " . $contentDisposition);
                },
            ]);

            $body = $resp->getBody();
            // Read bytes off of the stream until the end of the stream is reached
            while (!$body->eof()) {
                echo $body->read(1024);
            }
        });

        // add gzip encoding
        $response->headers->set('Content-Encoding', 'gzip');
        $response->send();

        return $response;
    }

    /**
     * @param Flux $flux
     * 
     * @return ResponseInterface
     */
    public function delete(Flux $flux)
    {
        $url = sprintf('consumer/%d', $flux->getId());
        return $this->client->delete($url);
    }

    /**
     * Generate a JsonResponse based on RequestException.
     *
     * @param RequestException $e
     *
     * @return JsonResponse
     */
    public static function handleRequestException(RequestException $e)
    {
        if ($response = $e->getResponse()) {
            return JsonResponse::fromJsonString($response->getBody()->getContents(), $e->getCode());
        } else {
            return JsonResponse::create(['error' => ['code' => 504, 'message' => 'Aucune réponse du serveur de traitement.']], 504);
        }
    }
}
