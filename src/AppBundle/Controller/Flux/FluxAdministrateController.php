<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Flux\Flux;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/flux/{id}/administrate", requirements={"id": "\d+"})
 * @Security("is_granted('flux.edit', flux)")
 */
class FluxAdministrateController extends Controller
{
    /**
     * @Route("", name="flux.administrate")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return Response
     */
    public function indexAction(Request $request, Flux $flux)
    {
        return $this->render('flux/administrate.html.twig', array(
            'flux' => $flux,
        ));
    }

    /**
     * @Route("/delete", name="flux.administrate.delete")
     * @Security("is_granted('flux.delete', flux)")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, Flux $flux)
    {
        $em = $this->getDoctrine()->getManager();
        if ('POST' === $request->getMethod()) {
            $em->remove($flux);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.flux_deleted'));

            return new JsonResponse([
                'success' => true, 'redirect' => $this->generateUrl('flux.index'),
            ], 200);
        }

        return $this->render('flux/administrate/delete.html.twig', [
            'flux' => $flux,
        ]);
    }
}
