<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Application\Application;
use AppBundle\Entity\Flux\Process;
use AppBundle\Entity\User\User;
use AppBundle\Entity\Flux\Download;
use AppBundle\Entity\Flux\Flux;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Security("is_granted('flux.download', flux)")
 * @Route(options={ "i18n"= false })
 */
class FluxDownloadController extends Controller
{
    /**
     * Modal to inform user about the flux status.
     *
     * @Route("/flux/{id}/download/partial", name="flux.download.partial")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function partialAction(Flux $flux, Request $request)
    {
        $process = $flux->getLastSuccessProcess(Process::TYPE_PARTIAL);
        if ($process && $process->isRunning()) {
            throw new BadRequestHttpException('Un process est déjà en cours');
        }
        $ready = $process && ($process->getChecksum() == $flux->getChecksum());
        if ('POST' === $request->getMethod()) {
            return JsonResponse::create([
                'redirect' => $this->generateUrl('flux.download.partial.get', array('id' => $flux->getId())),
            ]);
        }
        $template = $ready ? 'download' : 'generate';

        return $this->render('flux/download/partial/'.$template.'.html.twig', [
            'flux' => $flux,
            'process' => $process,
        ]);
    }

    /**
     * Download or generate partial file.
     *
     * @Route("/flux/{id}/download/partial/get", name="flux.download.partial.get")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function partialGetAction(Flux $flux, Request $request)
    {
        $process = $flux->getLastProcess(Process::TYPE_PARTIAL);
        if ($process && $process->isRunning()) {
            throw new BadRequestHttpException('Un process est déjà en cours');
        }
        $ready = $process && ($process->getChecksum() == $flux->getChecksum());
        if ($ready) {
            // download
            return $this->get('app.java_worker.client')->download($flux, Process::TYPE_PARTIAL);
        } else {
            // generate
            $this->get('app.process_cache')->generateFluxCacheData($flux, Process::TYPE_PARTIAL);
            $this->addFlash('success', $this->get('translator')->trans('msg.partial_feed_generate'));

            return $this->redirect($request->headers->get('referer'));
        }
    }

    /**
     * Modal to inform user about the complete download.
     *
     * @Route("/flux/{id}/download/complete", name="flux.download.complete")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function completeAction(Flux $flux, Request $request)
    {
        $process = $flux->getLastSuccessProcess(Process::TYPE_COMPLETE);
        if (!$process) {
            throw new Exception("Le flux n'a pas encore été exécuté.");
        }

        if ('POST' === $request->getMethod()) {
            return JsonResponse::create([
                'redirect' => $this->generateUrl('flux.download.complete.get', array('id' => $flux->getId())),
            ]);
        }

        return $this->render('flux/download/complete/download.html.twig', [
            'flux' => $flux,
            'process' => $process,
        ]);
    }

    /**
     * Download complete file.
     *
     * @Route("/flux/{id}/download/complete/get", name="flux.download.complete.get")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function completeGetAction(Flux $flux, Request $request)
    {
        $process = $flux->getLastSuccessProcess(Process::TYPE_COMPLETE);
        $em = $this->getDoctrine()->getManager();
        if (!$process) {
            throw new Exception('No action available');
        }

        // download
        if (!$this->isGranted('ROLE_ADMIN', $this->getUser())) {
            // save download entity
            $process = $flux->getLastSuccessProcess(Process::TYPE_COMPLETE);
            $download = new Download($process);
            $download->setType(Download::DOWNLOAD_MANUAL);
            $em->persist($download);
            $em->flush();
        }

        return $this->get('app.java_worker.client')->download($flux, Process::TYPE_COMPLETE);
    }

    /**
     * Modal to validate scheduling.
     *
     * @Route("/flux/{id}/download/schedule", name="flux.download.schedule")
     *
     * @param Flux    $flux
     * @param Request $request
     *
     * @return Response
     */
    public function scheduleAction(Flux $flux, Request $request)
    {
        $process = $flux->getLastSuccessProcess(Process::TYPE_COMPLETE);
        if (null !== $flux->getScheduleHour()) {
            throw new Exception($this->get('translator')->trans('msg.api_scheduled'));
        }

        if ('POST' === $request->getMethod()) {
            $em = $this->getDoctrine()->getManager();
            $hour = rand(0, 23);
            $flux->setScheduleHour($hour);
            $em->flush();
            $this->addFlash('success', $this->get('translator')->trans('msg.complete_feed_schedule'));

            return JsonResponse::create([
                'reload' => true,
            ]);
        }

        return $this->render('flux/download/complete/schedule.html.twig', [
            'flux' => $flux,
            'process' => $process,
        ]);
    }

    /**
     * Return file via webservice.
     *
     * @Route("/webservice/{flux_key}/{app_key}", name="flux.download.webservice")
     * @Security("is_granted('IS_AUTHENTICATED_ANONYMOUSLY')")
     *
     * @param $flux_key
     * @param $app_key
     *
     * @return BinaryFileResponse | JsonResponse
     */
    public function webserviceAction($flux_key, $app_key)
    {
        $fluxRepository = $this->getDoctrine()->getRepository(Flux::class);
        /** @var Flux $flux */
        $flux = $fluxRepository->findOneBy(['apiId' => $flux_key]);
        if (!$flux || !$this->get('security.authorization_checker')->isGranted('flux.download', $flux)) {
            return new JsonResponse(array('message' => $this->get('translator')->trans('msg.api_access_denied')), 403);
        }

        $user = $flux->getUser();
        $applicationRepository = $this->getDoctrine()->getRepository(Application::class);
        /** @var Application $application */
        $application = $applicationRepository->findOneBy([
            'user' => $user, 
            'apiKey' => $app_key,
            'deleted' => false,
        ]);
        if (!$application) {
            return new JsonResponse(array('message' => $this->get('translator')->trans('msg.api_access_denied')), 403);
        }

        $process = $flux->getLastSuccessProcess(Process::TYPE_COMPLETE);
        if (!$process) {
            if (!$flux->getScheduleHour()) {
                return new JsonResponse(array(
                    'message' => $this->get('translator')->trans('msg.api_unscheduled'),
                ), 503);
            } else {
                return new JsonResponse(array(
                    'message' => $this->get('translator')->trans('msg.api_generating'),
                ), 503);
            }
        }

        $em = $this->getDoctrine()->getManager();
        $download = new Download($process);
        $download->setType(Download::DOWNLOAD_WEBSERVICE);
        $download->setApplication($application);
        $em->persist($download);

        if (!$user->isAccountNonExpired()) {
            $user->setAccountNonExpired(true);
        }

        $em->flush();

        try {
            return $this->get('app.java_worker.client')->download($flux, Process::TYPE_COMPLETE);
        } catch (\Exception $e) {
            $this->get('logger')->err($e->getMessage());
            return new JsonResponse(array(
                'message' => $this->get('translator')->trans('msg.server_error'),
            ), 500);
        }
    }
}
