<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Controller\Flux;

use AppBundle\Entity\Flux\Download;
use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\Process;
use AppBundle\Form\Filter\ProcessFilter;
use AppBundle\Form\Type\Flux\FluxType;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/flux/{id}", requirements={"id": "\d+"})
 * @Security("is_granted('flux.see', flux)")
 */
class FluxDetailController extends Controller
{
    /**
     * @Route("/parameters", name="flux.parameters")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return Response
     */
    public function parametersAction(Request $request, Flux $flux)
    {
        $form = $this->createForm(FluxType::class, $flux);
        if ($this->isGranted('flux.edit', $flux)) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->flush();
                $this->addFlash('success', $this->get('translator')->trans('msg.flux_updated', ['%name%' => $flux->getName()]));

                return $this->redirectToRoute('flux.parameters', array('id' => $flux->getId()));
            }
        }

        return $this->render('flux/detail/parameters.html.twig', [
            'form' => $form->createView(),
            'flux' => $flux,
        ]);
    }

    /**
     * @Route("/query", name="flux.query")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return Response
     */
    public function queryAction(Request $request, Flux $flux)
    {
        return $this->render('flux/detail/query.html.twig', [
            'flux' => $flux,
        ]);
    }

    /**
     * @Route("/processes", name="flux.processes")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return Response
     */
    public function processesAction(Flux $flux, Request $request): Response
    {
        $pageSize = 15;
        $paginationOptions = array(
            'defaultSortFieldName' => 'process.createdAt',
            'defaultSortDirection' => 'desc',
        );

        /** @var QueryBuilder $qb */
        $qb = $this->get('app.repository.process')->createPaginationQueryBuilder($flux, Process::TYPE_COMPLETE);

        $list = $this->get('webapp.filterable_paginated_list_factory')
            ->getList($qb, ProcessFilter::class)
            ->setPageSize($pageSize)
            ->setPaginationOptions($paginationOptions)
            ->handleRequest($request);

        $items = $list->getItems();

        return $this->render('flux/detail/processes.html.twig', array(
            'flux' => $flux,
            'list' => $list,
            'items' => $items,
            'page_size' => $pageSize,
        ));
    }

    /**
     * @Route("/downloads", name="flux.downloads")
     *
     * @param Request $request
     * @param Flux    $flux
     *
     * @return Response
     */
    public function downloadsAction(Request $request, Flux $flux)
    {
        $pageSize = 20;
        $query = $this->get('doctrine.orm.entity_manager')
            ->getRepository(Download::class)
            ->createQueryBuilder('d')
            ->leftJoin('AppBundle:Flux\Flux', 'f', Join::WITH, 'd.flux = f')
            ->leftJoin('AppBundle:Application\Application', 'a', Join::WITH, 'd.application = a')
            ->where('f = :flux')->setParameter('flux', $flux)
            ->addGroupBy('d.id')
            ->addGroupBy('f.id')
            ->addGroupBy('a.id');

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate($query, $request->query->getInt('page', 1), $pageSize, [
            'distinct' => false,
            'defaultSortFieldName' => 'd.createdAt',
            'defaultSortDirection' => 'DESC',
        ]);

        return $this->render('flux/detail/downloads.html.twig', array(
            'list' => $list,
            'items' => $list->getItems(),
            'page_size' => $pageSize,
            'flux' => $flux,
        ));
    }
}
