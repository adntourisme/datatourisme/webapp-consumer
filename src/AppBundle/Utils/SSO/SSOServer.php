<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Utils\SSO;

use AppBundle\Entity\User\User;

class SSOServer
{
    private $payload;
    private $secret;
    private $parameters = [];

    public function __construct(SSOPayload $payload, String $secret)
    {
        $this->payload = $payload;
        $this->secret = $secret;
    }

    public function setUserData(User $user)
    {
        $nonce = $this->payload->getNonce();

        $this->parameters = [
            'nonce' => $nonce,
            'external_id' => $user->getId(),
            'email' => $user->getEmail(),
            'name' => $user->getFullName(),
        //    'avatar_url'  => $model->getAvatar(),
        ];
    }

    public function build()
    {
        $string = base64_encode(http_build_query($this->parameters));
        $data = [
            'sso' => $string,
            'sig' => $this->payload->encode($string, $this->secret),
        ];

        return http_build_query($data);
    }
}
