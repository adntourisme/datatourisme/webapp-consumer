<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Utils;

/**
 * Service designed to analyze and format string SPARQL 1.1 queries.
 */
class SparqlReviewer
{
    /**
     * @param string $query
     *
     * @return array
     */
    public function getQueryErrors(string $query)
    {
        return array();
    }

    /**
     * @param string $query
     *
     * @return bool
     */
    public function hasOrderBy(string $query)
    {
        $matches = array();
        preg_match('/ORDER\\s+BY/i', strtolower($query), $matches);

        return count($matches) > 0;
    }

    /**
     * @param string $query
     *
     * @return bool
     */
    public function hasService(string $query)
    {
        return false;
    }
}
