<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Validator\Constraints;

use AppBundle\Utils\SparqlReviewer;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Class SparqlRequestValidator.
 */
class SparqlRequestValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     */
    public function validate($query, Constraint $constraint)
    {
        if (!$constraint instanceof SparqlRequest) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\SparqlRequest');
        }

        // not null constraint
        if (0 === strlen($query)) {
            $this->context->buildViolation($constraint->emptyRequestMessage)
                ->setCode(SparqlRequest::EMPTY_REQUEST)
                ->addViolation();

            return;
        }

        $sparqlReviewer = new SparqlReviewer();

        // query validity constraint
        $errors = $sparqlReviewer->getQueryErrors($query);
        if (!empty($errors)) {
            $this->context->buildViolation($constraint->invalidRequestMessage)
                ->setCode(SparqlRequest::INVALID_REQUEST)
                ->addViolation();

            return;
        }

        // order by constraint
        if ($sparqlReviewer->hasOrderBy($query)) {
            $this->context->buildViolation($constraint->forbiddenOrderByMessage)
                ->setCode(SparqlRequest::ORDER_BY_KEYWORD)
                ->addViolation();

            return;
        }

        // service constraint
        if ($sparqlReviewer->hasService($query)) {
            $this->context->buildViolation($constraint->forbiddenServiceMessage)
                ->setCode(SparqlRequest::SERVICE_KEYWORD)
                ->addViolation();

            return;
        }
    }
}
