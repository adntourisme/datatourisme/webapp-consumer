<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class SparqlRequest.
 */
class SparqlRequest extends Constraint
{
    const EMPTY_REQUEST = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce2000';
    const ORDER_BY_KEYWORD = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce2001';
    const SERVICE_KEYWORD = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce2002';
    const INVALID_REQUEST = 'e2a3fb6e-7ddc-4210-8fbf-2ab345ce2003';

    protected static $errorNames = array(
        self::EMPTY_REQUEST => 'EMPTY_REQUEST_ERROR',
        self::ORDER_BY_KEYWORD => 'ORDER_BY_KEYWORD_ERROR',
        self::SERVICE_KEYWORD => 'SERVICE_KEYWORD_ERROR',
        self::INVALID_REQUEST => 'INVALID_REQUEST_ERROR',
    );

    public $emptyRequestMessage = 'You have to fill-in a request';
    public $forbiddenOrderByMessage = 'You can not order requests';
    public $forbiddenServiceMessage = 'You can not use services';
    public $invalidRequestMessage = 'The request is invalid';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}
