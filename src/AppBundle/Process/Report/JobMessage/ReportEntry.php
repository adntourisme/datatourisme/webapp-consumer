<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\JobMessage;

class ReportEntry
{
    const LEVEL_FATAL = 'FATAL';
    const LEVEL_INFO = 'INFO';

    private $message;
    private $stage;
    private $level;
    private $exception;
    private $data;
    private $reports;

    public function __construct()
    {
        $this->reports = new \ArrayObject();
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     *
     * @return $this
     */
    public function setMessage(string $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * @param mixed $stage
     *
     * @return $this
     */
    public function setStage(string $stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     *
     * @return $this
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param mixed $exception
     *
     * @return $this
     */
    public function setException($exception)
    {
        $this->exception = $exception;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @param $r
     *
     * @return ReportEntry
     */
    public function setEntry($r)
    {
        isset($r['message']) ? $this->setMessage($r['message']) : '';
        isset($r['data']) ? $this->setData($r['data']) : '';
        isset($r['level']) ? $this->setLevel($r['level']) : '';
        isset($r['exception']) ? $this->setException($r['exception']) : '';
        isset($r['reports']) ? $this->setReports($r['reports']) : '';

        return $this;
    }

    /**
     * @return \ArrayObject|ReportEntry[] ReportEntry
     */
    public function getReports(): \ArrayObject
    {
        return $this->reports;
    }

    /**
     * @param array $reports
     *
     * @return $this
     */
    public function setReports(array $reports)
    {
        foreach ($reports as $report) {
            $reportEntry = new self();
            $reportEntry->setEntry($report);
            $this->reports->append($reportEntry);
        }

        return $this;
    }
}
