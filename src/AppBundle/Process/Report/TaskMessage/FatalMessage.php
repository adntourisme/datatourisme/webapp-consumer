<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report\TaskMessage;

use AppBundle\Entity\Flux\Process;
use AppBundle\Entity\Log;
use AppBundle\Process\Report\JobMessage\ReportEntry;

class FatalMessage extends AbstractTaskMessage
{
    public function process(ReportEntry $report)
    {
        $log = new Log();
        $log->setProcess($this->process);
        $log->setLevel(Log::LOG_FATAL);
        if (empty($report->getMessage()) && '' !== $report->getStage()) {
            $log->setMessage($report->getStage());
        } else {
            $log->setMessage((string) $report->getMessage());
        }
        $this->em->persist($log);

        // create process
        $this->process->setStatus(Process::STATUS_ERROR);

        return $this->process;
    }
}
