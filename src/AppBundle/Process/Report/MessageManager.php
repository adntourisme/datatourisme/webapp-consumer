<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Process\Report;

use AppBundle\Entity\Flux\Process;
use AppBundle\Process\Report\JobMessage\ReportEntry;
use AppBundle\Process\Report\TaskMessage\FatalMessage;
use AppBundle\Process\Report\TaskMessage\ProcessQueryResultMessage;
use Doctrine\ORM\EntityManager;

class MessageManager
{
    protected $uuid;
    protected $task;
    protected $stats;
    /** @var \ArrayObject $report */
    private $reports;

    public function __construct($data)
    {
        $this->stats = new \ArrayObject();
        $this->reports = new \ArrayObject();
        isset($data['uuid']) ? $this->setUuid($data['uuid']) : '';
        isset($data['stats']) ? $this->setStats($data['stats']) : '';
        isset($data['task']) ? $this->setTask($data['task']) : '';
        isset($data['reports']) ? $this->setReports($data['reports']) : '';
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return ArrayObject
     */
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * @param mixed $stats
     */
    public function setStats($stats)
    {
        foreach ($stats as $key => $stat) {
            $this->stats->offsetSet($key, $stat);
        }
    }

    /**
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param string $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return mixed
     */
    public function getReports(): \ArrayObject
    {
        return $this->reports;
    }

    /**
     * @param array $reports
     */
    public function setReports($reports)
    {
        foreach ($reports as $step => $report) {
            $reportEntry = new ReportEntry();
            $reportEntry->setStage($step)->setEntry($report);
            $this->reports->append($reportEntry);
        }
    }

    /**
     * @return bool
     *
     * @throws \Exception
     */
    public function process(EntityManager $em, Process $process)
    {
        // update process last job
        $process->setLastJob($this->getStats()->offsetGet('id'));

        $reports = $this->getReports();
        // get the last report and stop execution if fatal
        if ($reports && $reports->count()) {
            $job = $reports[$reports->count() - 1];
            if (ReportEntry::LEVEL_FATAL == $job->getLevel()) {
                $fatal = new FatalMessage($em, $process);
                $fatal->process($job);

                return $process; // Fatal error, stop the loop
            }
        }

        foreach ($reports as $job) {
            switch ($job->getStage()) {
//                case 'delete':
//                    $em->remove($process->getFlux());
//                    break;
                case 'process_graph_result':
                case 'process_tuple_result':
                    $processResult = new ProcessQueryResultMessage($em, $process);
                    $process = $processResult->process($job);
                    break;
            }
        }

        return $process;
    }
}
