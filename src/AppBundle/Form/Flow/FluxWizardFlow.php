<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Flow;

use AppBundle\Entity\Flux\OutputType;
use AppBundle\Form\Type\Flux\FluxWizardType;
use Craue\FormFlowBundle\Form\FormFlow;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class FluxWizardFlow.
 */
class FluxWizardFlow extends FormFlow
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var TranslatorInterface
     */
    protected $translator;
    
    protected $allowDynamicStepNavigation = true;
	// protected $allowRedirectAfterSubmit = true;

    public function __construct(EntityManager $entityManager, TranslatorInterface $translator) {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    protected function loadStepsConfig()
    {
        return [
            [
                'label' => $this->translator->trans('label.wizard_flux.step_1'),
                'form_type' => FluxWizardType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ], 
            [
                'label' => $this->translator->trans('label.wizard_flux.step_2'),
                'form_type' => FluxWizardType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ], 
            [
                'label' => $this->translator->trans('label.wizard_flux.step_3'),
                'form_type' => FluxWizardType::class,
                'form_options' => [
                    'validation_groups' => ['Default'],
                ],
            ], 
            [
                'label' => $this->translator->trans('label.wizard_flux.step_4'),
                'form_type' => FluxWizardType::class,
                'form_options' => [
                    'validation_groups' => ['create'],
                ],
            ],
        ];
    }

    /**
     * @see https://github.com/craue/CraueFormFlowBundle/issues/250
     */
    public function generateFormObject()
    {
        $repository = $this->entityManager->getRepository(OutputType::class);
        $outputType = $repository->findOneBy(['requestType' => OutputType::TYPE_CONSTRUCT, 'default' => true]);
        $formData = new \stdClass();
        $formData->types = null;
        $formData->locations = null;
        $formData->outputType = $outputType;
        $formData->name = null;
        $formData->description = null;
        return $formData;
    }
}
