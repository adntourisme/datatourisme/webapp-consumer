<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\Flux;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ProcessFilter.
 */
class ProcessFilter extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultPlaceholder = 'placeholder.all';

//        $builder
//            // ____ PROCESS DATE
//            ->add('process_date', TextFilterType::class, array(
//                'apply_filter' => false,
//            ))
//
//            // ____ ORGANIZATION (Removed on PRE_SET_DATA if mandatory right is not granted)
//            ->add('organization', EntityFilterType::class, array(
//                'class' => Organization::class,
//                'placeholder' => $defaultPlaceholder,
//                'query_builder' => function (EntityRepository $er): QueryBuilder {
//                    return $er->createQueryBuilder('organization')
//                        ->join('organization.flux', 'flux')
//                        ->join('flux.processes', 'processes')
//                        ->having('COUNT (processes) > 0')
//                        ->groupBy('organization.id')
//                        ->orderBy('organization.name', 'ASC')
//                    ;
//                },
//                'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values) {
//                    if (empty($values['value'])) {
//                        return null;
//                    } elseif (false === $values['value'] instanceof Organization) {
//                        $errorValue = gettype($values['value']) !== 'object' ? gettype($values['value']) : get_class($values['value']);
//                        throw new \Exception('"organization" field\'s value must be instance of '.Organization::class.', '.$errorValue.' given.');
//                    }
//
//                    $eb = $filterQuery->getExpressionBuilder();
//                    $expression = $eb->expr()->orX(
//                        $eb->stringLike('organization.name', $values['value'], FilterOperands::STRING_EQUALS)
//                    );
//
//                    return $filterQuery->createCondition($expression);
//                },
//                'choice_value' => function ($value) {
//                    return $value instanceof Organization ? $value : null;
//                },
//            ))
//
//            // ____ FLUX
//            ->add('flux', TextFilterType::class, array(
//                'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values) {
//                    if (empty($values['value'])) {
//                        return null;
//                    }
//
//                    $eb = $filterQuery->getExpressionBuilder();
//                    $expression = $eb->expr()->orX(
//                        $eb->stringLike('flux.name', $values['value'], FilterOperands::STRING_CONTAINS)
//                    );
//
//                    return $filterQuery->createCondition($expression);
//                },
//            ))
//
//            // ____ MODE
//            ->add('mode', TextFilterType::class, array(
//                'apply_filter' => false,
//            ))
//
//            // ____ STATUS
//            ->add('status', ChoiceFilterType::class, array(
//                'placeholder' => $defaultPlaceholder,
//                'choices' => (function (): array {
//                    $choices = array();
//                    $processStatusList = Process::statusList();
//                    foreach ($processStatusList as $processStatus) {
//                        $choices[Process::$statusDefinitions[$processStatus][0]] = $processStatus;
//                    }
//                    ksort($choices);
//
//                    return $choices;
//                })(),
//            ))
//
//            // ____ FILE COUNT
//            ->add('file_count', TextFilterType::class, array(
//                'apply_filter' => false,
//            ))
//
//            // ____ DURATION
//            ->add('duration', TextFilterType::class, array(
//                'apply_filter' => false,
//            ))
//
//            // ____ PUBLISHED RATE
//            ->add('published_rate', TextFilterType::class, array(
//                'apply_filter' => false,
//            ))
//
//            // ____ EVOLUTION
//            ->add('evolution', TextFilterType::class, array(
//                'apply_filter' => false,
//            ))
//
//            // ____ ANOMALY COUNT
//            ->add('anomaly_count', TextFilterType::class, array(
//                'apply_filter' => false,
//            ))
//
//
//        ;
    }
}
