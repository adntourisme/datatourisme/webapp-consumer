<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Filter;

use AppBundle\Entity\Flux\Process;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Expr\Orx;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\FilterOperands;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use AppBundle\Entity\Flux\OutputType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

/**
 * Class FluxFilter.
 */
class FluxFilter extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $defaultPlaceholder = 'placeholder.all';

        $builder->add('name', TextFilterType::class);
        if (true === $options['userIsAdmin']) {
            $builder->add('fullName', TextFilterType::class, array(
                'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                    if (empty($values['value'])) {
                        return null;
                    }
                    // expressions that represent the condition
                    $qb = $filterQuery->getExpressionBuilder();

                    /** @var Orx $expression */
                    $expression = $qb->expr()->orX(
                        $qb->stringLike('u.lastName', $values['value'], FilterOperands::STRING_CONTAINS),
                        $qb->stringLike('u.firstName', $values['value'], FilterOperands::STRING_CONTAINS)
                    );

                    // special : search by user id
                    if (is_numeric($values['value'])) {
                        $expression->add(
                            $qb->expr()->eq('u.id', (int) $values['value'])
                        );
                    }

                    return $filterQuery->createCondition($expression);
                },
            ));
        } else {
            $builder->add('description', TextFilterType::class);
        }
        $builder->add('outputType', EntityFilterType::class, array(
            'class' => OutputType::class,
            'placeholder' => $defaultPlaceholder,
            'query_builder' => function (EntityRepository $er): QueryBuilder {
                return $er->createQueryBuilder('outputType')
                    ->groupBy('outputType.id')
                    ->orderBy('outputType.name', 'ASC');
            },
        ));

        $builder->add('lastProcessAt', TextFilterType::class, array(
            'apply_filter' => false,
        ));

        $builder->add('status', ChoiceFilterType::class, array(
            'placeholder' => $defaultPlaceholder,
            'apply_filter' => function (ORMQuery $filterQuery, string $field, array $values) {
                if (empty($values['value'])) {
                    return null;
                }

                $qb = $filterQuery->getQueryBuilder();
                $qb->leftJoin(Process::class, 'p', Join::WITH, 'p.flux = f AND p.type = :type');
                $qb->setParameter('type', Process::TYPE_COMPLETE);

                if ('disabled' == $values['value']) {
                    $expression = $qb->expr()->andX(
                        $qb->expr()->isNotNull('p.id'),
                        $qb->expr()->isNull('f.scheduleHour')
                    );

                    return $filterQuery->createCondition($expression);
                } else {
                    $qb->andHaving($qb->expr()->eq(
                        $qb->expr()->substring(
                            $qb->expr()->max(
                                $qb->expr()->concat('p.createdAt', 'p.status')
                            ),
                        20), ':status'));
                    $filterQuery->getQueryBuilder()->setParameter('status', $values['value']);

                    return $filterQuery->createCondition($qb->expr()->isNotNull('f.scheduleHour'));
                }
            },
            'choices' => (function (): array {
                $choices = array('label.status.deactivated' => 'disabled');
                foreach (Process::$statusDefinitions as $key => $def) {
                    $choices[$def[0]] = $key;
                }
                ksort($choices);

                return $choices;
            })(),
        ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired(array('userIsAdmin'));
    }
}
