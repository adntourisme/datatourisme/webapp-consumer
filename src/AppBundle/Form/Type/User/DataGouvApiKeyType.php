<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type\User;

use AppBundle\Entity\User\User;
use AppBundle\Utils\DataGouvApi;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DataGouvApiKeyType.
 */
class DataGouvApiKeyType extends AbstractType
{
    /** @var DataGouvApi */
    protected $dataGouvApi;

    /**
     * DataGouvApiKeyType constructor.
     *
     * @param DataGouvApi $dataGouvApi
     */
    public function __construct(DataGouvApi $dataGouvApi)
    {
        $this->dataGouvApi = $dataGouvApi;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dataGouvApiKey', TextareaType::class, array(
            'label' => 'label.datagouv_api_key',
            'required' => false,
            'attr' => array(
                'rows' => 3,
                'placeholder' => 'label.datagouv_api_key',
            ),
        ));

        $builder->addEventListener(FormEvents::POST_SUBMIT, array($this, 'onPostSubmit'));
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $apiKey = $form->get('dataGouvApiKey')->getData();

        if (!empty($apiKey) && -1 === $this->dataGouvApi->me($apiKey)) {
            $form->get('dataGouvApiKey')->addError(new FormError('La clé renseignée est invalide.'));
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
