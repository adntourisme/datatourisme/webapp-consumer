<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type\User;

use AppBundle\Entity\User\User;
use AppBundle\Form\Type\LocaleType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\User\UserType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Datatourisme\Bundle\WebAppBundle\Form\RoleType;

/**
 * Class AccountType.
 */
class AccountType extends AbstractType
{
    /** @var EntityManager */
    protected $em;

    /** @var AuthorizationCheckerInterface */
    protected $authorizationChecker;

    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var RequestStack */
    protected $requestStack;

    /**
     * RegisterType constructor.
     *
     * @param EntityManager                 $em
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TokenStorageInterface         $tokenStorage
     * @param RequestStack                  $requestStack
     */
    public function __construct(EntityManager $em, AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $tokenStorage, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $organizationTypes = join(',', $this->getOrganizationTypeIdList());

        $locale = $this->requestStack->getCurrentRequest()->getLocale();

        $builder
            ->add('lastName', TextType::class, array(
                'label' => 'label.lastname',
                'attr' => array(
                    'placeholder' => 'label.lastname',
                ),
            ))
            ->add('firstName', TextType::class, array(
                'label' => 'label.firstname',
                'attr' => array(
                    'placeholder' => 'label.firstname',
                ),
            ))
            ->add('email', EmailType::class, array(
                'label' => 'label.email',
                'attr' => array(
                    'placeholder' => 'label.email',
                ),
            ))
            ->add('zip', TextType::class, array(
                'label' => 'label.zipcode',
                'constraints' => new NotBlank(),
                'attr' => array(
                    'placeholder' => 'label.zipcode',
                ),
            ))
            ->add('country', CountryType::class, array(
                'label' => 'label.country',
                'required' => true,
                'placeholder' => 'placeholder.country',
                'preferred_choices' => 'fr' == $locale ? array('FR') : array(),
            ))
            ->add('type', EntityType::class, array(
                'label' => 'label.profile',
                'required' => true,
                'placeholder' => 'placeholder.profile',
                'class' => UserType::class,
                'choice_translation_domain' => 'entities',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')->orderBy('u.weight');
                },
            ))
            ->add('organization', TextType::class, array(
                'label' => 'label.organization',
                'required' => false,
                'group_attr' => array(
                    'data-visibility-target' => '#account_type',
                    'data-visibility-action' => 'show',
                    'data-visibility-value' => $organizationTypes,
                    'class' => 'required',
                ),
                'attr' => array(
                    'placeholder' => 'placeholder.organization',
                ),
            ))
            ->add('website', UrlType::class, array(
                'label' => 'label.website',
                'required' => false,
                'group_attr' => array(
                    'data-visibility-target' => '#account_type',
                    'data-visibility-action' => 'show',
                    'data-visibility-value' => $organizationTypes,
                    'class' => 'required',
                ),
                'attr' => array(
                    'placeholder' => 'label.website',
                ),
            ))
            ->add('locale', LocaleType::class, array(
                'label' => 'label.locale',
                'required' => true,
                'help' => 'help.account_locale',
            ))
        ;

        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $builder->add('role', RoleType::class, array(
                'choice_label' => array(
                    'ROLE_ADMIN' => 'Administrateur',
                    'ROLE_SUPER_ADMIN' => 'Super administrateur',
                    'ROLE_USER' => 'Diffuseur',
                    'ROLE_USER_CERTIFIED' => 'Diffuseur certifié',
                ),
            ));
        }

        $builder->addEventListener(FormEvents::POST_SET_DATA, array($this, 'onPostSetData'));
    }

    /**
     * getOrganizationTypeIdList.
     */
    protected function getOrganizationTypeIdList()
    {
        return array_map(function (UserType $type) {
            return $type->getId();
        }, $this->em->getRepository(UserType::class)->findBy(['isOrganization' => true]));
    }

    /**
     * @param FormEvent $event
     */
    public function onPostSetData(FormEvent $event)
    {
        /** @var User $user */
        $user = $event->getData();
        $form = $event->getForm();

        $isAdmin = strstr($user->getRole(), '_ADMIN');

        // for admin : remove some field
        if ($isAdmin) {
            $event->getForm()->remove('zip');
            $event->getForm()->remove('country');
            $event->getForm()->remove('type');
            $event->getForm()->remove('organization');
            $event->getForm()->remove('website');
        }

        if ($form->has('role')) {
            if ($this->tokenStorage->getToken()->getUser() == $user || ($isAdmin && !$this->authorizationChecker->isGranted('ROLE_SUPER_ADMIN'))) {
                $form->remove('role');
            } else {
                $options = $form->get('role')->getConfig()->getOptions();
                $options['choices'] = array_filter($options['choices'], function ($role) use ($isAdmin) {
                    return strstr($role, $isAdmin ? '_ADMIN' : '_USER');
                });
                $form->add('role', RoleType::class, $options);
            }
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}
