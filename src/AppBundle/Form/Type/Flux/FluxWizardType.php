<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type\Flux;

use AppBundle\Entity\Flux\OutputType;
use AppBundle\Ontology\OntologyFactory;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class FluxWizardType.
 */
class FluxWizardType extends AbstractType
{
	/**
	 * @var OntologyFactory
	 */
	protected $ontologyFactory;

    /**
     * @var TranslatorInterface
     */
	protected $translator;
	
    /**
     * FluxWizardType constructor.
     *
     * @param OntologyFactory $ontologyFactory
     * @param TranslatorInterface $translator
     */
    public function __construct(OntologyFactory $ontologyFactory, TranslatorInterface $translator)
    {
		$this->ontologyFactory = $ontologyFactory;
		$this->translator = $translator;
    }

	/**
	 * buildForm
	 */
    public function buildForm(FormBuilderInterface $builder, array $options) {
		switch ($options['flow_step']) {
			case 1:
				$builder->add('types', ChoiceType::class, [
					'choices' => $this->getTypeChoices(),
					'block_name' => 'checkboxes',
					'multiple' => true,
					'expanded' => true,
					'required' => true,
					'attr' => [
						'data-all-label' => $this->translator->trans('label.wizard_flux.types_all'),
						'data-cols-cut' => "3"
					]
				]);

				break;

			case 2:
				// This form type is not defined in the example.
				$builder->add('locations', ChoiceType::class, [
					'choices' => $this->getLocationChoices(),
					'block_name' => 'checkboxes',
					'multiple' => true,
					'expanded' => true,
					'required' => true,
					'attr' => [
						'data-all-label' => $this->translator->trans('label.wizard_flux.locations_all'),
						'data-cols-cut' => "5,9"
						//'data-cols-cut' => "6,11"
					]
				]);
				break;

			case 3:
				$builder->add('outputType', EntityType::class, [
					'query_builder' => function (EntityRepository $er) {
						return $er->createQueryBuilder('ot')
							->where('ot.requestType = :requestType OR ot.requestType is null')
							->andWhere('ot.code LIKE \'bundle/%\'')
							->orderBy('ot.default', 'DESC')
							->setParameter('requestType', OutputType::TYPE_CONSTRUCT);
					},
					'class' => OutputType::class,
					'group_by' => 'type',
					'expanded' => true
				]);
				break;

			case 4:
				$builder
					->add('name', TextType::class, ['label' => 'label.name'])
					->add('description', TextareaType::class, ['label' => 'label.description', 'attr' => ['rows' => 3]]);
				break;
		}
	}

	/**
	 * Build the type choices from JavaWorker
	 */
	private function getTypeChoices() {
		$ontology = $this->ontologyFactory->create('lite');

		// get the first level
		$firstLevel = $ontology->getSubClasses(':PointOfInterest');

		$icons = [
			":EntertainmentAndEvent" => "calendar",
			":PlaceOfInterest" => "flag",
			":Product" => "euro",
			":Tour" => "map-signs",
		];

		// build the second level
		$choices = array_combine(
			// keys
			array_map (function($class) use ($icons) { 
				return '<i class="fa fa-' . $icons[$class['uri']] . '"></i> ' . $class['label']; 
			}, $firstLevel), 
			// values
			array_map (function($class) use ($ontology) {
				return array_combine(
					// keys
					array_map (function($res) { return $res['label']; }, $ontology->getSubClasses($class['uri'])), 
					// values
					array_map (function($res) { return $res['uri']; }, $ontology->getSubClasses($class['uri']))
				);
			}, $firstLevel));
		return $choices;
	}

	/**
	 * Build the type choices from JavaWorker
	 */
	private function getLocationChoices() {
		$ontology = $this->ontologyFactory->create();
		$individuals = $ontology->getIndividuals(':Department');

		$groups = [];
		$dom = [];
		foreach($individuals as $individual) {
			$uri = $individual['uri'];
			$insee = $individual['properties'][':insee'][0];
			$label = $individual['label'];
			$region = $individual['properties'][':isPartOfRegion'][0];

			if (strlen($insee) > 2) {
				// DOM/TOM
				$dom[$insee . " - " . $label] = $uri;
			} else {
				$groups[$ontology->getLabel($region)][$insee . " - " . $label] = $uri;
			}
		}

		ksort($groups);
		foreach($groups as &$choices) {
			ksort($choices);
		}
		$groups['DOM/TOM'] = $dom;

		return $groups;
	}
}
