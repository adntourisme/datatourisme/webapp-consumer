<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type\Flux;

use AppBundle\Validator\Constraints\SparqlRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SparqlType.
 */
class SparqlType extends AbstractType
{
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'label' => false,
            'options' => array(),
            'constraints' => array(
                new SparqlRequest(array(
                    'emptyRequestMessage' => 'Vous devez renseigner une requête.',
                    'forbiddenOrderByMessage' => 'La requête ne doit pas contenir de directive ORDER BY.',
                    'forbiddenServiceMessage' => 'Le mot clé SERVICE ne doit pas être utilisé.',
                    'invalidRequestMessage' => 'La requête n\'est pas valide.',
                )),
            ),
        ));
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $attr = $view->vars['attr'];
        $attr['data-sparql-editor'] = json_encode($options['options']);
        $view->vars['attr'] = $attr;
    }

    /**
     * @return string
     */
    public function getParent(): string
    {
        return TextareaType::class;
    }
}
