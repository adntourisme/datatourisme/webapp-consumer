<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use AppBundle\Entity\Application\Application;
use AppBundle\Entity\Application\ApplicationType as Type;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ApplicationType.
 */
class ApplicationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'label.name',
                'attr' => array(
                    'placeholder' => 'placeholder.application_name',
                ),
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'label.description',
                'attr' => array(
                    'rows' => 5,
                    'placeholder' => 'placeholder.application_description',
                ),
            ))
            ->add('type', EntityType::class, array(
                'label' => 'label.type',
                'choice_translation_domain' => 'entities',
                'class' => Type::class,
            ))
            ->add('url', UrlType::class, array(
                'label' => 'label.url',
                'attr' => array(
                    'placeholder' => 'placeholder.application_url',
                ),
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Application::class,
        ));
    }
}
