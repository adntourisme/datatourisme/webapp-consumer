<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Notification\Type;

use AppBundle\Entity\User\User;
use Datatourisme\Bundle\WebAppBundle\Notification\Type\AbstractType;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Symfony\Component\Intl\Intl;

class UserRegisterType extends AbstractType
{
    private $_em;

    public function __construct(EntityManager $em)
    {
        $this->_em = $em;
    }

    public function getMessage()
    {
        return "L'utilisateur <strong>{{ user.fullName }}</strong> vient de créer un compte.";
    }

    public function getDescription()
    {
        return <<<EOF
            <p>Voici le détail du compte :</p>
            <ul>
                <li><strong>Nom</strong>: {{ user.lastName }}</li>
                <li><strong>Prénom</strong>: {{ user.firstName }}</li>
                <li><strong>Email</strong>: {{ user.email }}</li>
                <li><strong>Code postal</strong>: {{ user.zip }}</li>
                <li><strong>Pays</strong>: {{ countryNames[user.country] }}</li>
                <li><strong>Profile</strong>: {{ user.type.name }}</li>
                {% if user.organization is not empty %}
                    <li><strong>Structure</strong>: {{ user.organization }}</li>
                {% endif %}
                {% if user.website is not empty %}
                    <li><strong>Site internet</strong>: <a href="{{ user.website }}">{{ user.website }}</a></li>
                {% endif %}
            </ul>
EOF;
    }

    public function getLevel()
    {
        return Logger::NOTICE;
    }

    public function getContext()
    {
        return array(
            'user' => $this->getSubject(),
            'countryNames' => Intl::getRegionBundle()->getCountryNames(),
        );
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return 'user.edit';
    }

    /**
     * @return array
     */
    public function getRouteParameters()
    {
        return array('id' => $this->getSubject()->getId());
    }

    public function getRecipients()
    {
        $recipients = $this->_em->getRepository(User::class)->findBy(['role' => ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']]);

        return $recipients;
    }
}
