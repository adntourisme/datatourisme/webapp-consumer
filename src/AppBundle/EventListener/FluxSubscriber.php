<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Flux\OutputType;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Mapping\PrePersist;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class FluxSubscriber.
 */
class FluxSubscriber implements EventSubscriber
{
    /** @var Container */
    private $container;

    /**
     * FluxSubscriber constructor.
     *
     * Container is injected to avoid ServiceCircularReferenceException
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getSubscribedEvents()
    {
        return array(
            'prePersist',
            'preUpdate',
            'preRemove',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var Flux $flux */
        $flux = $args->getObject();
        if ($flux instanceof Flux) {
            // add default language
            if (empty($flux->getLanguages())) {
                $defaultLocale = $this->container->getParameter('locale');
                /** @var RequestStack $requestStack */
                $requestStack = $this->container->get('request_stack');
                $languages = [$defaultLocale];
                if ($requestStack && $requestStack->getCurrentRequest() && $requestStack->getCurrentRequest()->getLocale()) {
                    $languages[] = $requestStack->getCurrentRequest()->getLocale();
                }
                $flux->setLanguages(array_unique($languages));
            }
        }
    }

    /**
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(PreUpdateEventArgs $args)
    {
        $flux = $args->getObject();
        if ($flux instanceof Flux) {
            $flux->setQueryType(null);

            // update output type
            if ($flux->getQuery()) {
                $queryType = OutputType::getTypeFromQuery($flux->getQuery());
                if (!$queryType) {
                    $flux->setQuery(null);
                    $flux->setOutputType(null);
                } else {
                    $flux->setQueryType($queryType);

                    // check outputype is compatible with query type
                    $outputType = $flux->getOutputType();
                    if (!$outputType || $outputType->getRequestType() != $queryType) {
                        // if not, change to default
                        $repo = $args->getEntityManager()->getRepository(OutputType::class);
                        $outputType = $repo->findOneBy(['requestType' => $queryType, 'default' => true]);
                        $flux->setOutputType($outputType);
                        //$this->session->getFlashBag()->add("info", "Le type de requête a changé : vous pouvez choisir un nouveau format de sortie");
                    }
                }
            } else {
                $flux->setOutputType(null);
            }

            // rundeck update if schedule changed
            if (in_array('scheduleHour', array_keys($args->getEntityChangeSet()))) {
                if (null == $flux->getScheduleHour()) {
                    $flux->setExecutedAt(null);
                }
                $flux->updateUsedAt();
                $this->container->get('app.process_cache')->updateRundeckCron($flux);
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $flux = $args->getObject();
        if ($flux instanceof Flux) {
            // delete rundeck task
            $this->container->get('app.task_manager')->delete($flux->getId());
            // delete flux storage in java worker
            $this->container->get('app.java_worker.client')->delete($flux);
        }
    }
}
