<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\GraphQLResolver;

use Conjecto\SemGraphQL\Resolver\BlazegraphResolver\BlazegraphResolver;
use Conjecto\SemGraphQL\Resolver\SparqlResolver\Collection\Collection;
use Conjecto\SemGraphQL\Resolver\SparqlResolver\Sparql\Select;
use Conjecto\SemGraphQL\Resolver\SparqlResolver\Sparql\Where;
use Conjecto\SemGraphQL\Resolver\SparqlResolver\Utils\SparqlUtils;
use Conjecto\SemGraphQL\Schema\AbstractSchema;
use Youshido\GraphQL\Execution\ResolveInfo;

/**
 * GraphQL resolver to get the subquery.
 *
 * @deprecated (not used anymore)
 */
class BlazegraphSubqueryResolver extends BlazegraphResolver
{
    public function resolve(string $entryPoint, $arguments, ResolveInfo $info)
    {
        /** @var AbstractSchema $schema */
        $schema = $info->getExecutionContext()->getSchema();
        SparqlUtils::$prefixMap = $schema->getPrefixMap();

        $filters = isset($arguments['filters']) ? $arguments['filters'] : [];
        $query = $this->getFilterGenerator()->getFilters($entryPoint, $filters, null);
        $sparql =
            (new Collection())
                ->add(new Select('DISTINCT ?s'.SparqlUtils::normalize($entryPoint).'1 '))
                ->add(new Where($query));

        return array(
            'query' => $sparql ? $sparql : null,
            'results' => [],
        );
    }
}
