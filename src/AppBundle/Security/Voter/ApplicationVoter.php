<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Security\Voter;

use AppBundle\Entity\Application\Application;
use AppBundle\Entity\User\User;
use Datatourisme\Bundle\WebAppBundle\Security\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ApplicationVoter extends AbstractVoter
{
    const SEE = 'application.see';
    const CREATE = 'application.create';
    const EDIT = 'application.edit';
    const REMOVE = 'application.remove';
    const SEE_API_KEY = 'application.see_api_key';

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        if (parent::supports($attribute, $subject)) {
            return null === $subject || $subject instanceof Application;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var User $account */
        $account = $token->getUser();
        switch ($attribute) {
            case self::SEE:
            case self::CREATE:
            case self::EDIT:
            case self::REMOVE:
            case self::SEE_API_KEY:
                return $this->canManage($account, $subject);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @param User             $account
     * @param Application|null $application
     *
     * @return bool
     */
    private function canManage(User $account, $application)
    {
        if (in_array($account->getRole(), array('ROLE_USER', 'ROLE_USER_CERTIFIED'))) {
            if (!$application) {
                return true;
            } elseif ($application->getUser()->getId() === $account->getId()) {
                return true;
            }
        }

        return false;
    }
}
