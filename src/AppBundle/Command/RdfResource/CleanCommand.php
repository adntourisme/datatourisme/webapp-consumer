<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\RdfResource;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('datatourisme:rdf-resource:clean')
            ->setDescription('Batch command to clean RdfResource table');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $conn = $em->getConnection();

        $date = new \DateTime();
        $date->modify('-1 month');
        
        $sql = "DELETE FROM rdf_resource WHERE flux_id IN (
            SELECT DISTINCT(flux.id) 
            FROM flux 
            LEFT JOIN download ON download.flux_id = flux.id 
            GROUP BY flux.id 
            HAVING MAX(download.created_at) IS NULL OR MAX(download.created_at) < :date
        )";
        $countDeleted = $conn->executeUpdate($sql, ['date' => $date->format('Y-m-d')]);
        $output->writeln("$countDeleted deleted entries");
    }
}
