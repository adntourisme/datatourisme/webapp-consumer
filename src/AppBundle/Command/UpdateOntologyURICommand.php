<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command;

use AppBundle\Entity\AlignmentRulesetRevision;
use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Organization;
use AppBundle\Entity\ThesaurusAlignment;
use ClassesWithParents\D;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateOntologyURICommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('datatourisme:update-ontology-uri')
            ->addArgument('old', InputArgument::REQUIRED, 'old uri')
            ->addArgument('new', InputArgument::REQUIRED, 'new uri');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oldUri = $input->getArgument('old');
        $newUri = $input->getArgument('new');
        $em = $this->getContainer()->get('doctrine')->getManager();

        // update flux
        $fluxx = $em->getRepository(Flux::class)->findAll();

        /* @var Flux $flux */
        foreach ($fluxx as $flux) {
            $output->writeln("Flux : " . $flux->getId());
            if($flux->getQuery()) {
                $query = $flux->getQuery();
                $query = preg_replace("/\b".preg_quote($oldUri, "/")."/", $newUri, $query);
                $flux->setQuery($query);
            }

            if($flux->getQueryData()) {
                $json = json_encode($flux->getQueryData());
                $json = preg_replace("/".preg_quote( "\"" . str_replace('/', '\/', $oldUri), "/")."/",
                    "\"" . str_replace('/', '\/', $newUri),
                    $json);
                $data = json_decode($json, true);
                $flux->setQueryData($data);
            }
        }

        $em->flush();
    }
}
