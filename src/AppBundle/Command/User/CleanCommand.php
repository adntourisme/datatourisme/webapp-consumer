<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace AppBundle\Command\User;

use AppBundle\Entity\User\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('datatourisme:user:clean')
            ->setDescription('Batch command to set accountNonExpired to false if account is inactive AND delete account when unused during more than 2y');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine')->getManager();
        $conn = $em->getConnection();
        $repository = $em->getRepository(User::class);

        // determine dates for comparison
        $deletionMaxDate = (new \DateTime())->modify('-2 years');
        $expirationMaxDate = (clone $deletionMaxDate)->modify('+ 1 week');
        $activationMaxDate = (new \DateTime())->modify('- 1 week');

        /** @var User[] $accounts */
        $countExpired = 0;
        $countDeleted = 0;
        $accounts = $repository->findBy(['role' => ['ROLE_USER', 'ROLE_USER_CERTIFIED']]);
        foreach ($accounts as $account) {
            $createdAt = $account->getCreatedAt();
            $lastLogin = $account->getLastLogin();

            // get last download date
            $sql = 'SELECT MAX(download.created_at) as lastDownload 
                FROM flux 
                LEFT JOIN download ON download.id = flux.last_download_id
                WHERE flux.user_id = :user_id';

            $stmt = $conn->executeQuery($sql, ['user_id' => $account->getId()]);
            $raw = $stmt->fetchColumn();
            $lastDownload = $raw ? new DateTime($raw) : null;

            // determiner latest date
            $dates = array_filter([$createdAt, $lastLogin, $lastDownload]);
            rsort($dates);
            $latest = current($dates);

            // si plus de 2 ans se sont écoulés et que le compte est expiré, on le supprime
            if (($latest < $deletionMaxDate) && !$account->isAccountNonExpired()) {
                $this->getContainer()->get('webapp.mailer')->send('account.expire_delete', $account);
                $em->remove($account);
                $countDeleted++;
                continue;
            }

            // si le compte est prêt à être supprimé et qu'il n'est pas expiré, envoyer un mail
            if (($latest < $expirationMaxDate) && $account->isAccountNonExpired()) {
                $this->getContainer()->get('webapp.mailer')->send('account.expire', $account);
                $account->setAccountNonExpired(false);
                $countExpired++;
                continue;
            }

            // si le compte n'a pas été activé 1 semaine aprés sa création, on le supprime
            if (($latest < $activationMaxDate) && !$account->isEnabled()) {
                $em->remove($account);
                $countDeleted++;
                continue;
            }
        }

        $em->flush();
        $output->writeln("$countDeleted deleted account(s)");
        $output->writeln("$countExpired expired account(s)");
    }
}
