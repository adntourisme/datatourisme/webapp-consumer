/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var gulp = require('gulp');
var gulp_scripts = require('./gulp/scripts');
var gulp_styles = require('./gulp/styles');
var gulp_assets = require('./gulp/assets');
var buster = require('gulp-buster');

// watches
gulp.task('watch:scripts', gulp_scripts.watch);
gulp.task('watch:styles', gulp_styles.watch);
gulp.task('watch', ['watch:scripts', 'watch:styles']);

// builds
gulp.task('build:scripts', gulp_scripts.build);
gulp.task('build:styles', gulp_styles.build);
gulp.task('build:assets', gulp_assets.build);
gulp.task('build', ['build:scripts', 'build:styles', 'build:assets'], function() {
    return gulp.src("web/assets/**/*")
        .pipe(buster({ relativePath: "web/" }))
        .pipe(gulp.dest('.'));
});

gulp.task('default', ['build']);