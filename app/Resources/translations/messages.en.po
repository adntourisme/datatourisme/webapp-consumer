# -----------------
# Global
# ----------------

msgid "public_header_label"
msgstr "<strong>Consumer</strong> app"

msgid "label.name"
msgstr "Name"

msgid "label.description"
msgstr "Description"

msgid "label.administration"
msgstr "Administration"

msgid "label.applications"
msgstr "Applications"

msgid "label.application"
msgstr "Application"

msgid "label.support"
msgstr "Help"

msgid "label.datagouv"
msgstr "Simplified exports"

msgid "label.type"
msgstr "Type"

msgid "label.users"
msgstr "Users"

msgid "label.my_profile"
msgstr "My profile"

msgid "label.publish_datagouv"
msgstr "Data.gouv.fr publication"

msgid "label.member_from"
msgstr "Member from %date%"

msgid "placeholder.all"
msgstr "-- All --"

msgid "label.step"
msgstr "Step"

# ---- langues

msgid "label.locale.fr"
msgstr "French"

msgid "label.locale.en"
msgstr "English"

msgid "label.locale.de"
msgstr "German"

msgid "label.locale.nl"
msgstr "Dutch"

msgid "label.locale.it"
msgstr "Italian"

msgid "label.locale.es"
msgstr "Spanish"

msgid "label.locale.ru"
msgstr "Russian"

msgid "label.locale.zh"
msgstr "Chinese"

# -----------------
# Security : login
# ----------------

msgid "text.login"
msgstr "Log in to start a session"

msgid "link.forgotten_password"
msgstr "I forgot my password !"

msgid "btn.login"
msgstr "Log in"

msgid "title.invite_register"
msgstr "No account yet?"

msgid "text.invite_register"
msgstr ""
"Please create your user account in order to access the platform in order "
"to download and reuse the available tourist data."

msgid "btn.register"
msgstr "Create an account"

# -----------------
# Security : password reset
# ----------------

msgid "title.password_reset"
msgstr "Reset password"

msgid "text.password_reset"
msgstr ""
"Fill the email address used during your registration to receive a reset link"

msgid "link.login_back"
msgstr "Return to the authentication form"

msgid "msg.reset_password_mail_sent"
msgstr "An email has been sent to you to reset your password"

msgid "msg.reset_password_success"
msgstr "Your password has been updated"

msgid "msg.reset_password_obsolete"
msgstr ""
"The link provided is no longer valid. You must make a new password reset request."

# -----------------
# Security : register
# ----------------

msgid "title.register"
msgstr "Create a consumer account"

msgid "text.register_1"
msgstr ""
"Please fill in the information below to create your user account"

msgid "text.register_2"
msgstr "Why create an account on the DATAtourisme platform?"

msgid "text.register_3"
msgstr ""
"The DATAtourisme platform allows its users to generate customized data "
"feeds, available in OpenData according to the terms of the Etalab 2.0 "
"license. The creation of an account is therefore essential to access "
"the custom settings and feed management screens."

msgid "text.register_4"
msgstr ""
"The information collected on this form is recorded in a computerized "
"file by the Direction Générale des Entreprises and is intended for sending "
"information relating to the DATAtourisme device, evaluating the uses of "
"the platform, as well as the fight against misuse of it."

msgid "text.register_5"
msgstr ""
"This data is kept for the life of the user account. In accordance with the "
"law \"Informatique et Libertés\", you can exercise your right of access to "
"the data concerning you and rectify them according to the modalities detailed "
"in the Terms of Use."

# -----------------
# Account form
# ----------------

msgid "label.lastname"
msgstr "Last name"

msgid "label.firstname"
msgstr "First name"

msgid "label.email"
msgstr "Email"

msgid "label.zipcode"
msgstr "Postal code"

msgid "label.profile"
msgstr "Profile"

msgid "placeholder.country"
msgstr "- Choose a country -"

msgid "placeholder.profile"
msgstr "- Choose a profile -"

msgid "label.organization"
msgstr "Organization"

msgid "placeholder.organization"
msgstr "Name of the organization"

msgid "label.website"
msgstr "Website"

msgid "label.country"
msgstr "Country"

msgid "label.locale"
msgstr "Language"

msgid "help.account_locale"
msgstr "Language used to send email notifications"

msgid "msg.register_mail_sent"
msgstr "An email has been sent to you to finalize your registration"

msgid "msg.account_updated"
msgstr "Your account has been updated"

msgid "msg.password_updated"
msgstr "Your password has been updated"

msgid "msg.key_saved"
msgstr "The key has been registered"

msgid "label.last_login"
msgstr "Last login"

msgid "label.status.blocked"
msgstr "Blocked"

msgid "label.status.expired"
msgstr "Expired"

msgid "label.status.certified"
msgstr "Certificated"

msgid "label.status.validated"
msgstr "Validated"

msgid "label.status.standard"
msgstr "Standard"

msgid "label.datagouv_api_key"
msgstr "data.gouv.fr API key"

msgid "text.datagouv_api_key"
msgstr "To be able to automatically publish your feeds on "
"<a href=\"%20\" target=\"_blank\">data.gouv.fr</a>, you must fill "
"in the API key provided by the service below. Once filled in, the "
"publishing option will be available in the settings of each of your feeds."

# ---- administrate

msgid "title.modify_password"
msgstr "Change my password"

msgid "help.modify_password"
msgstr "Start the procedure to change your password"

# -----------------
# CGU
# ----------------

msgid "label.cgu"
msgstr "Terms of Use"

msgid "option.cgu_accept"
msgstr "I accept the general terms of use"

msgid "msg.cgu_required"
msgstr ""
"You must accept the terms of use to use the platform."

msgid "btn.download_pdf_format"
msgstr "Download PDF"

msgid "text.cgu_quality_disclaimer"
msgstr "In particular, I understand that datatourisme.gouv.fr "
"ensures the aggregation and the valorization of third data "
"and that it belongs to the community of the producer members "
"to ensure the quality of the content which it produces"

msgid "btn.accept_cgu"
msgstr "I accept the terms of use"

# -----------------
# Homepage
# ----------------

msgid "title.welcome"
msgstr "Welcome to your diffuser area!"

msgid "text.welcome"
msgstr "<p>In order to be able to consume the data of the "
"DATAtourisme dataset, we invite you to configure your "
"customised feed(s) by clicking on the button below. "
"You can then consult and adjust the parameters of your "
"feeds at any time.</p>"
"<p>Need help in your steps? Feel free to refer to the "
"documentation and tools provided free of charge.</p>"
"<p>Finally, we remind you that DATAtourisme is a "
"collaborative system: we invite you to consult and "
"contribute regularly to the discussions of the "
"online <a href=\"https://support.datatourisme.gouv.fr/\" "
"target="\_blank\">mutual aid forum!</a></p>"

msgid "title.last_announcements"
msgstr "Last announcements"

msgid "title.last_topics"
msgstr "Last support topics"

msgid "label.goto_flux_index"
msgstr "Go to my feeds list"

msgid "label.create_first_flux"
msgstr "Create my first feed"

msgid "label.more_announcements"
msgstr "All announcements"

msgid "label.more_topics"
msgstr "All support topics"

msgid "label.subject"
msgstr "Subject"

msgid "label.replies"
msgstr "Replies"

msgid "label.views"
msgstr "Views"

msgid "label.activity"
msgstr "Activity"

# -----------------
# Flux index
# ----------------

msgid "label.flux"
msgstr "Feed"

msgid "label.flux_plural"
msgstr "Feeds"

msgid "label.new_flux"
msgstr "New feed"

msgid "label.create_flux"
msgstr "Create a new feed"

msgid "label.user"
msgstr "User"

msgid "label.format"
msgstr "Format"

msgid "label.update"
msgstr "Update"

msgid "label.status"
msgstr "Status"

msgid "label.status.deactivated"
msgstr "Disabled"

msgid "label.status.wait"
msgstr "Waiting"

msgid "text.status.wait"
msgstr "The process is pending"

msgid "label.status.running"
msgstr "In progress"

msgid "text.status.running"
msgstr "The process is in progress"

msgid "label.status.ok"
msgstr "Operational"

msgid "text.status.ok"
msgstr "The process is operational"

msgid "label.status.error"
msgstr "Error"

msgid "text.status.error"
msgstr "The process encountered a blocking error"

msgid "btn.partial_feed_running"
msgstr "Excerpt generation in progress"

msgid "text.partial_feed_running"
msgstr "The excerpt of the feed is being generated"

msgid "btn.partial_feed_download"
msgstr "Download an excerpt"

msgid "btn.partial_feed_generate"
msgstr "Generate new excerpt"

msgid "msg.partial_feed_generate"
msgstr "The excerpt file is under construction: you will be notified by email when it will be downloadable"

msgid "msg.feed_query_required"
msgstr "You must first design the feed query"

msgid "msg.feed_authorized_required"
msgstr "You will be able to download the data when your account is validated"

msgid "msg.feed_available_required"
msgstr "You will be able to download the data once it has been generated"

msgid "btn.complete_feed_download"
msgstr "Download the latest version"

msgid "btn.complete_feed_schedule"
msgstr "Schedule generation"

msgid "msg.complete_feed_schedule"
msgstr "The feed generation has been scheduled: you will be notified by email when the first version will be downloadable"

msgid "text.flux_no_result"
msgstr "No feed matches your search"

msgid "label.download_download_report"
msgstr "Download a download report"

msgid "title.download_feed"
msgstr "Download a feed"

msgid "title.download_feed_excerpt"
msgstr "Download an excerpt"

msgid "text.download_feed_1"
msgstr "You are about to download the latest version of the feed generated by the platform"

msgid "text.download_feed_2"
msgstr "It is important to note that this version <strong> does not necessarily correspond "
"to the current state of the setting of your feed </strong> if it has been modified or "
"scheduled recently."

msgid "text.download_feed_excerpt_1"
msgstr "You are about to download an excerpt of the platform-generated feed"

msgid "text.download_feed_excerpt_2"
msgstr "This excerpt is <strong> limited to 10 resources or 1000 triplets </strong>. This "
"implies that some POI <strong> may be incomplete </strong>"

msgid "label.date"
msgstr "Date"

msgid "title.schedule_feed"
msgstr "Schedule the daily generation"

msgid "label.schedule"
msgstr "Schedule"

msgid "text.schedule_feed_1"
msgstr "You are about to schedule the generation of your feed that will be executed "
"<strong>daily</strong>."

msgid "text.schedule_feed_2"
msgstr "At the end of this first generation, <strong>an email will be sent to you for "
"notify you of file availability</strong>."

msgid "text.schedule_feed_3"
msgstr "Subsequently, the feed data will be regenerated <strong>automatically "
"every day </strong>. You will find in the <strong> download</strong> tab "
"the version history of your feed"

msgid "text.schedule_feed_4"
msgstr "In the meantime, you can get <strong>an excerpt</strong> from the "
"<strong>Query Editor</strong> tab of your feed"

msgid "title.schedule_feed_excerpt"
msgstr "Schedule the generation of an excerpt"

msgid "btn.schedule_now"
msgstr "Schedule now!"

msgid "text.schedule_feed_excerpt_1"
msgstr "You are about to schedule the generation of an excerpt of your feed. "
"Extracting data from the feed is a <strong>operation that may take some time </strong>, "
"depending on the complexity of the query."

msgid "text.schedule_feed_excerpt_2"
msgstr "Once the excerpt is ready, <strong> an email will be sent to inform you </strong>."

msgid "text.schedule_feed_excerpt_3"
msgstr "This excerpt is <strong>limited to 10 resources or 1000 triples</strong>. "
"This implies that some POI <strong>may be incomplete </strong>."

msgid "title.output_example_limit"
msgstr "5 objects example"

msgid "text.output_big_file"
msgstr "With this format, the feed generates <strong>a single file</strong> "
"containing all the points of interest : it can therefore quickly <strong>grow up</strong> "
"depending on the quantity of objects selected."

msgid "text.output_bundle_file.intro"
msgstr "The stream generated a ZIP archive containing several %format% files:"

msgid "text.output_bundle_file.1"
msgstr "An <strong>index file</strong>, listing the points of interest contained in the archive, with their last modification date in the platform"

msgid "text.output_bundle_file.2"
msgstr "An <i>objects</i> folder containing, within an iterative hierarchy of folders, <strong>a file for each point of interest</strong>"

msgid "text.output_bundle_file.3"
msgstr "A <strong>context file</strong>, allowing you to transform each file into <a href="https://json-ld.org/" target="_blank">JSON-LD</a>, if necessary"

# -----------------
# Flux create
# ----------------

msgid "btn.create_flux"
msgstr "Create the feed"

msgid "msg.flux_created"
msgstr "The feed has been created"

# -----------------
# Flux wizard
# ----------------

msgid "label.wizard_flux"
msgstr "Feed creation wizard"

msgid "label.wizard_flux.count"
msgstr "<strong>0</strong> points of interest"

msgid "label.wizard_flux.step_1"
msgstr "Select the types of point of interest"

msgid "label.wizard_flux.step_2"
msgstr "Determine geographic coverage"

msgid "label.wizard_flux.step_3"
msgstr "Choose an output format"

msgid "label.wizard_flux.step_4"
msgstr "Finalize and create the feed"

msgid "label.wizard_flux.types_all"
msgstr "All types of point of interest"

msgid "label.wizard_flux.locations_all"
msgstr "All the regions"

msgid "text.wizard_flux.step1_help"
msgstr "<strong>Please note</strong> : it will be possible to select the categories more finely by applying an additional filter once the initial configuration of the feed has been completed."

msgid "text.wizard_flux.step2_help"
msgstr "<strong>Please note</strong> : it will be possible to select the desired geographic coverage more finely by applying an additional filter once the initial feed configuration has been completed."

msgid "text.wizard_flux.step3_help"
msgstr "<strong>Please note</strong>: other more technical export formats will be available once the initial feed configuration is completed."

msgid "btn.wizard_flux.next_step"
msgstr "Next step >"

msgid "btn.wizard_flux.prev_step"
msgstr "< Previous step"

# -----------------
# Flux detail
# ----------------

msgid "title.flux"
msgstr "Feed: %name%"

msgid "msg.scheduling_deactivated"
msgstr "Automatic feed generation has been disabled. You can re-schedule it using the "
"<span class=\"label label-primary\">Schedule generation</span> button."

msgid "tab.parameters"
msgstr "Settings"

msgid "tab.query_editor"
msgstr "Query editor"

msgid "tab.sparql_query"
msgstr "SPARQL query"

msgid "tab.processes"
msgstr "Download"

msgid "tab.downloads"
msgstr "History"

msgid "label.locales"
msgstr "Language(s)"

msgid "label.output_format"
msgstr "Format"

msgid "help.output_format"
msgstr "The proposed formats depend on the SPARQL query type (SELECT or CONSTRUCT)"

msgid "help.output_format_need_query"
msgstr "You must therefore save your request before you can select a format."

msgid "help.output_format_preview_link"
msgstr "See an example of the selected format."

msgid "label.webservice"
msgstr "Webservice"

msgid "help.webservice"
msgstr "To consume the feed from one of your applications, you must use the "
"address above to replace the <strong> {app_key} </strong> setting with the "
"API key for the app, which you'll find in the <strong><a href=\"%url%\" "
"target=\"_blank\">Applications</a> section </strong>"

msgid "msg.flux_updated"
msgstr "The %name% feed has been updated"

msgid "label.publish_datagouv"
msgstr "Publish on data.gouv.fr"

msgid "help.publish_datagouv_missing_datasets"
msgstr "You must fill <a href=\"%url_datagouv%\" target=\"_blank\">your API "
"key</a> with data.gouv.fr in <a href=\"%url%\" target=\"_blank\">your profile"
"</a> to be able to automatically publish the data to a dataset of your "
"data.gouv.fr account."

msgid "help.publish_datagouv"
msgstr "This option allows you to publish this feed automatically and at regular "
"intervals to one of the datasets of your data.gouv.fr account"

msgid "label.preview_format"
msgstr "Preview format"

msgid "label.datagouv_dataset"
msgstr "Dataset"

# ---- query

msgid "btn.open_visual_editor"
msgstr "Open the visual editor"

msgid "btn.switch_to_expert"
msgstr "Switch to expert mode"

msgid "msg.query_updated"
msgstr "The query has been updated: it will be used in the next generation of the feed"

msgid "title.switch_to_expert"
msgstr "Enabling expert mode"

msgid "text.switch_to_expert.1"
msgstr "You are about to move the query editor to expert mode. This mode allows you to "
"directly edit the query in <a href=\"%url%\" target=\"_blank\">SPARQL language</a>"

msgid "text.switch_to_expert.2"
msgstr "Once expert mode is enabled, <strong>you will not be able to return to visual "
"mode</strong>. This operation is <strong>irreversible</strong>"

msgid "btn.confirm_switch_to_expert"
msgstr "I understand, I switch to expert mode"

msgid "btn.preview_results"
msgstr "Preview the results"

# ---- editor

msgid "label.visual_editor"
msgstr "Visual query editor"

msgid "text.visual_editor_loading"
msgstr "Loading..."

msgid "btn.save_query"
msgstr "Save the query"

# ---- processes

msgid "label.date_hour"
msgstr "Date/Hour"

msgid "label.size"
msgstr "Size"

msgid "label.poi_count"
msgstr "Number of POIs"

msgid "help.process_poi_count"
msgstr "Number of POIs extracted during processing"

msgid "label.download_link"
msgstr "Download link"

msgid "help.process_format"
msgstr "Output format of the generated file"

msgid "help.process_size"
msgstr "File size"

msgid "label.status.generating"
msgstr "Generation in progress"

msgid "label.status.preparing_generation"
msgstr "Preparation of the generation"

msgid "label.status.scheduled"
msgstr "Scheduled at %time%"

msgid "btn.download_last_version"
msgstr "Download the latest version"

msgid "text.processes_no_result"
msgstr "No version has been generated yet"

msgid "help.processes_no_result"
msgstr "To start generating your feed, click the <span class=\"label label-primary\"> "
"Schedule generation</span> button in the header of the page"

# ---- downloads

msgid "text.downloads_no_result"
msgstr "No download associated with this feed"

msgid "label.download_datetime"
msgstr "Download"

msgid "help.download_datetime"
msgstr "Date and time of feed download"

msgid "label.download_method"
msgstr "Method"

msgid "help.download_method"
msgstr "Feed download method"

msgid "label.download_app"
msgstr "Application"

msgid "help.download_app"
msgstr "Application that downloaded the feed"

msgid "label.download_type_manual"
msgstr "Manual"

msgid "help.download_type_manual"
msgstr "The feed is manually downloaded"

msgid "label.download_type_webservice"
msgstr "Webservice"

msgid "help.download_type_webservice"
msgstr "The feed is downloaded by a webservice"

# ---- administration

msgid "label.delete_flux"
msgstr "Delete the feed"

msgid "help.delete_flux"
msgstr "Permanently delete the stream from the platform"

msgid "title.delete_flux"
msgstr "Delete the %name% feed"

msgid "btn.delete_flux_confirm"
msgstr "Delete the feed and associated data"

msgid "text.warning"
msgstr "Warning !"

msgid "text.delete_flux.1"
msgstr "You are about to permanently delete the <strong>%name% feed and all associated data</​strong>"

msgid "text.delete_flux.2"
msgstr "This is an <strong>irreversible</strong> operation"

msgid "msg.flux_deleted"
msgstr "The feed and all associated data have been deleted"

# ---- API

msgid "msg.api_access_denied"
msgstr "Access denied"

msgid "msg.api_scheduled"
msgstr "The generation is already scheduled"

msgid "msg.api_unscheduled"
msgstr "Feed generation has not yet been scheduled or has been disabled due to a period of inactivity"

msgid "msg.api_generating"
msgstr "The generation is being scheduled. You will receive an email as soon as it is available for download"

msgid "msg.server_error"
msgstr "The server encountered an error"

# -----------------
# Application
# ----------------

msgid "title.application_create"
msgstr "Add an application"

msgid "title.application_delete"
msgstr "Delete the %name% application"

msgid "msg.application_updated"
msgstr "The %name% application has been successfully saved"

msgid "label.url"
msgstr "URL"

msgid "label.api_key"
msgstr "API key"

msgid "help.api_key"
msgstr "This API key allows your application to consume your feeds "
"through their webservice. Check the details of one of your feeds "
"for more information"

msgid "text.applications_no_result"
msgstr "You do not have an application yet."

msgid "help.applications_no_result"
msgstr "Too bad :( But you can <a href=\"%url%\" data-toggle=\"modal-remote\"> create one </a>!"

msgid "placeholder.application_name"
msgstr "Name of the application"

msgid "placeholder.application_description"
msgstr "Description of the application"

msgid "placeholder.application_url"
msgstr "URL of the application"

msgid "text.delete_application.1"
msgstr "You are about to permanently delete the <strong>%name%</strong> application and the associated API key."

msgid "text.delete_application.2"
msgstr "This is an <strong>irreversible</strong> operation : you will not be able to use this API key to download your feeds."

msgid "btn.delete_application_confirm"
msgstr "Delete the application and the associated API key"

msgid "msg.application_deleted"
msgstr "The application and the associated API key have been deleted"

# -----------------
# Administration
# ----------------

msgid "text.users_no_result"
msgstr "No user matches your search"

msgid "msg.admin_created"
msgstr "The administrator has been created: an email has been sent to him to "
"invite him to finalize the procedure"

msgid "msg.user_updated"
msgstr "The user has been updated"

msgid "msg.user_certified"
msgstr "The user has been certified"

msgid "msg.user_uncertified"
msgstr "The user has been uncertified"

msgid "msg.user_blocked"
msgstr "The user has been blocked"

msgid "msg.user_unblocked"
msgstr "The user has been unblocked"

msgid "msg.user_deleted"
msgstr "The user has been deleted"

msgid "btn.download_list"
msgstr "Download the list"

msgid "label.lastname_firstname"
msgstr "First - last"

msgid "label.new_admin"
msgstr "New administrator"

msgid "label.select_periode"
msgstr "Select a period"

