/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var $form = $("[name=flux_wizard]");
var $widget = $(".wizard-multiselect-widget");
var $counter = $(".wizard-result-counter");

var original;
var count = 0;

window._init_wizard_script = function(object) {
    original = object;

    // update stats
    var debounce = $.debounce(updateStats, 1000);
    var update = function() {
        $(".wizard-result-counter").addClass("busy"); 
        updateCheckboxes();
        debounce();
    }
    
    $('[name="flux_wizard[types][]"]', $form).change(update);
    $('[name="flux_wizard[locations][]"]', $form).change(update);

    $(".checkall", $widget).click(function() {
        var checked = $(this).is(':checked');
        $("input[type=checkbox]", $widget).prop( "checked", checked );
        update();
    });
    
    $(".checkallsub", $widget).click(function() {
        var checked = $(this).is(':checked');
        $("input[type=checkbox]", $(this).parents("ul")).prop( "checked", checked );
        update();
    });

    update();

    $(".wizard-radio-wrapper input[type=radio]").click(updateRadios);
    updateRadios();
}

/**
 * updateStats
 **/
var updateStats = function() {
    // data merge
    var formArray = $form.serializeArray();

    var override = {};
    $.map(formArray, function(n, i){
        const regex = /flux_wizard\[([^\]]+)\]/g;
        const match = regex.exec(n['name']);
        if (match && typeof original[match[1]] != "undefined") {
            if (!override[match[1]]) {
                override[match[1]] = [];
            }
            override[match[1]].push(n['value']);
        }
    });
    
    var data = {};
    for (var attr in original) { data[attr] = original[attr]; }
    for (var attr in override) { data[attr] = override[attr]; }
    
    // call ajax
    $.ajax({
        url: "/flux/create/preview", 
        type: "POST",
        data: JSON.stringify(data), 
        contentType: "application/json; charset=utf-8", 
        success: function(response) {
            $(".wizard-result-counter").show().removeClass("busy");
            $(".wizard-result-counter strong")
            .prop('counter', count)
            .animate({ counter: response.count }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now).toLocaleString('fr'));
                }
            });
            count = response.count;
        },
        dataType: "json"
    });
}

/** 
 * updateCheckboxes
 **/
var updateCheckboxes = function() {
    $(".checkall", $widget).prop("checked", !$("li.indent input[type=checkbox]:not(:checked)", $widget).length);
    $(".checkallsub", $widget).each(function() {
        $(this).prop("checked", !$("li.indent input[type=checkbox]:not(:checked)", $(this).parents("ul")).length);
    })
}

/** 
 * updateRadios
 **/
var updateRadios = function() {
    $(".wizard-radio-wrapper").each(function() {
        var checked = $("input[type=radio]", $(this)).is(':checked');
        $(this)[checked ? 'addClass' : 'removeClass']('active');
    });
}
