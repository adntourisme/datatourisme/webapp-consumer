/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

var $ = require("jquery");
var CodeMirror = window.CodeMirror = require('codemirror');
require("codemirror/mode/javascript/javascript");
require("codemirror/mode/xml/xml");
require("codemirror/mode/turtle/turtle");
require("codemirror/mode/sparql/sparql");

$.fn.initComponents.add(function(dom) {
    $('[data-codemirror]', dom).each(function() {
        var $this = $(this),
            $code = $this.html(),
            $unescaped = $('<div/>').html($code).text();

        var language = $this.data('codemirror');

        $this.empty();
        var e = CodeMirror(this, {
            value: $unescaped,
            matchBrackets: true,
            autoCloseBrackets: true,
            mode: language,
            lineWrapping: true,
            readOnly: true
        });
        //editor.setSize('100%', 100)
        setTimeout(function () {
            e.refresh();
            setTimeout(function () { e.refresh(); }, 100); // call it twice to avoid extra spaces
        }, 200);
    });
});