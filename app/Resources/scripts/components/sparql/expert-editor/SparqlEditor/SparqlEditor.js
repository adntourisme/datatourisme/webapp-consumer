/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
"use strict";

var YASQE = require('yasgui-yasqe');

/**
 * SparqlEditor
 * @param ontology
 * @param textarea
 * @param options
 * @constructor
 */
function SparqlEditor(ontology, textarea, options) {
    this.options = function () {
        var defaultOptions = {
            matchBrackets: true,
            lineNumbers: true,
            createShareLink: false,
            fullScreen: false,
            showQueryButton: false,
            persistent: null
        };
        var appOptions = typeof options !== "undefined" ? options : {};
        return $.extend(defaultOptions, appOptions);
    }();

    this.ontology = ontology;
    this.editor = this.initYasqe(textarea);
}

/**
 * Defines default configurations for Yasqe, such as autocompleters
 * @param textarea
 * @returns {doc|*}
 */
SparqlEditor.prototype.initYasqe = function(textarea) {

    // COMMON OPTIONS
    var optiOptions = {"async": true, "bulk": true, "autoShow": true};

    // CUSTOM PROPERTIES
    var properties = Object.keys(this.ontology.properties);
    YASQE.registerAutocompleter("customPropertyCompleter", function(yasqe) {
        return $.extend({
            "get": function(token, callback) {
                callback(properties);
            },
            "isValidCompletionPosition": function() {
                return YASQE.Autocompleters.properties.isValidCompletionPosition(yasqe);
            },
            "preProcessToken": function(token) {
                return token;
            },
            "persistent": "rdf_properties"
        }, optiOptions)
    });

    // CUSTOM PREFIXES
    var context = this.ontology["@context"];
    var prefixes = function() {
        var prefixes = [];
        Object.keys(context).map(function(objectKey, index) {
            prefixes.push(String.format("{0}: <{1}>", objectKey, context[objectKey]));
        });
        return prefixes;
    }();
    YASQE.Autocompleters._prefixes = YASQE.Autocompleters.prefixes;
    YASQE.Autocompleters.prefixes = function(yasqe, completerName) {
        var completer = YASQE.Autocompleters._prefixes(yasqe, completerName);
        completer.async = true;
        completer.bulk = true;
        completer.autoShow = true;
        completer.get = prefixes;
        completer.persistent = "rdf_prefixes";
        return completer;
    };
    $.extend(YASQE.Autocompleters.prefixes, optiOptions, {"persistent": "prefixes"});

    var classes = Object.keys(this.ontology.classes);
    YASQE.registerAutocompleter("customClassCompleter", function(yasqe) {
        return $.extend({
            "get": function(token, callback) {
                callback(classes);
            },
            "isValidCompletionPosition": function() {
                return YASQE.Autocompleters.classes.isValidCompletionPosition(yasqe);
            },
            "preProcessToken": function(token) {
                return token;
            },
            "persistent": "rdf_classes"
        }, optiOptions)
    });

    // OVERRIDING DEFAULTS
    YASQE.defaults.autocompleters = ["customPropertyCompleter", "prefixes", "customClassCompleter"];

    return YASQE.fromTextArea(textarea[0], this.options);
};

/**
 * @param url
 * @param submitEl
 * @param frameEl
 */
SparqlEditor.prototype.watchForPreview = function(url, submitEl, frameEl) {
    var _this = this;
    submitEl.click(function() {
        $.ajax({
            type: "GET",
            url: url,
            data: {
                "sparqlq": _this.editor.getValue()
            },
            beforeSend: function() {
                frameEl.html(_this.getLoadingTemplate());
            },
            error: function() {
                frameEl.html(_this.getFailTemplate());
            },
            success: function(data) {
                frameEl.html(data);
            }
        });
    });
};

/**
 * @param options
 */
SparqlEditor.prototype.addOptions = function(options) {
    if (typeof options === "object")
        $.extend(this.options, options);

    return this;
};

/**
 * @param trigger
 */
SparqlEditor.prototype.timeout = function(trigger) {
    var editor = this.editor;
    setTimeout(function() {
        editor.refresh();
    }, trigger);
};

/**
 * @returns {string}
 */
SparqlEditor.prototype.getLoadingTemplate = function() {
    return '\
        <div class="text-center m-15">\
            <span class="fa fa-spinner fa-pulse"></span>\
            &nbsp;Exécution de la requête...\
        </div>\
    ';
};

/**
 * @returns {string}
 */
SparqlEditor.prototype.getFailTemplate = function() {
    return '\
        <div class="text-center m-15">\
            <span class="fa fa-warning"></span>\
            &nbsp;La requête a échoué.\
        </div>\
    ';
};

module.exports = SparqlEditor;