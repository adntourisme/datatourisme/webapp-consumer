<?php
/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace Tests\AppBundle\Context;

use AppBundle\Entity\User\User;
use Behat\Mink\Exception\ExpectationException;
use Behat\MinkExtension\Context\RawMinkContext;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Flux\Flux;
use AppBundle\Entity\Application\Application;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class FeatureContext extends RawMinkContext implements KernelAwareContext
{
    /** @var ContainerInterface */
    private $container;

    public function setKernel(KernelInterface $kernel)
    {
        $this->container = $kernel->getContainer();
    }

    /**
     * @Then I download the data file for flux :id
     *
     * @param int $id
     *
     * @throws NotFoundHttpException
     */
    public function downloadDataFile(int $id)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $flux = $em->getRepository('AppBundle:Flux\Flux')->find($id);
        if ($flux) {
            $userApplications = $em->getRepository('AppBundle:Application\Application')->findBy(array('user' => $flux->getUser()));
            if (count($userApplications) > 0) {
                /** @var Application $application */
                $application = $userApplications[rand(0, count($userApplications) - 1)];
                $url = $this->container->get('router')->generate(
                    'flux.download.webservice',
                    array('flux_key' => $flux->getApiId(), 'app_key' => $application->getApiKey(),
                    UrlGeneratorInterface::ABSOLUTE_URL,
                ));
                $url = str_replace('/app_test.php', '', $url);
                $this->visitPath($url);

                return;
            }
        }

        throw new NotFoundHttpException();
    }

    /**
     * @Then I fill in the data.gouv.fr ApiKey
     *
     * @throws ExpectationException
     */
    public function fillInDataGouvApiKey()
    {
        $this->getSession()->getPage()->fillField('data_gouv_api_key[dataGouvApiKey]', $this->container->getParameter('dataGouvApiKey'));
    }

    /**
     * @Then I push the file :file to flux :id dataset
     *
     * @throws ExpectationException
     */
    public function pushDataGouvFile($file, $id)
    {
        if (!$this->container->getParameter('datagouvapikey')) {
            return;
        }

        if ($this->getMinkParameter('files_path')) {
            $fullPath = rtrim(realpath($this->getMinkParameter('files_path')), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$file;
            if (is_file($fullPath)) {
                $file = $fullPath;
            }
        }

        $url = $this->container->get('router')->generate('internal.datagouv.push', array('id' => $id, 'file' => $file), UrlGeneratorInterface::ABSOLUTE_URL);
        parent::visitPath($url);

        return;
    }

    /**
     * @Then I should have :number file for flux :id dataset
     *
     * @throws ExpectationException
     */
    public function assertNumberDatasetFiles($number, $id)
    {
        if (!$this->container->getParameter('datagouvapikey')) {
            return $number;
        }

        $flux = $this->getFlux($id);
        $this->setConnectedUser('certified@dt.dev');
        $resources = $this->container->get('app.data_gouv_api')->getDataSetResources($flux->getDataSet());

        if (-1 !== $resources && count($resources) !== (int) $number) {
            throw new ExpectationException('Dataset has '.count($resources).' files. '.$number.' where expected', $this->getSession()->getDriver());
        }
    }

    /**
     * @Then I clean all dataset files for flux :id
     *
     * @throws ExpectationException
     */
    public function cleanDatasetFiles($id)
    {
        $this->setConnectedUser('certified@dt.dev');
        $this->container->get('app.data_gouv_api')->clean($this->getFlux($id)->getDataSet());
    }

    /**
     * @Then the flux :id should be active
     *
     * @param int $id
     *
     * @throws ExpectationException
     */
    public function fluxIsActive(int $id)
    {
        $this->isFluxActive($id, true);
    }

    /**
     * @Then the flux :id should be unactive
     *
     * @param int $id
     *
     * @throws ExpectationException
     */
    public function fluxIsUnactive(int $id)
    {
        $this->isFluxActive($id, false);
    }

    /**
     * @param $id
     * @param $activation
     *
     * @throws ExpectationException
     */
    protected function isFluxActive($id, $activation)
    {
        $flux = $this->getFlux($id);
        if (!$flux) {
            throw new NotFoundHttpException();
        }

        if ($flux->isActive() !== $activation) {
            throw new ExpectationException('Current flux should be '.($activation ? 'active' : 'unactive'), $this->getSession()->getDriver());
        }
    }

    /**
     * @param $email
     */
    protected function setConnectedUser($email)
    {
        $user = $this->getUser($email);
        $token = new UsernamePasswordToken($user, null, 'user_db', $user->getRoles());
        $this->container->get('security.token_storage')->setToken($token);
    }

    /**
     * @param $id
     *
     * @return Flux
     */
    protected function getFlux($id)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $flux = $em->getRepository('AppBundle:Flux\Flux')->find($id);

        return $flux;
    }

    /**
     * @param $email
     *
     * @return User
     */
    protected function getUser($email)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User\User')->findOneByEmail($email);

        return $user;
    }
}
