Feature: DAT-3 Réinitialisation du mot de passe
  En tant qu'utilisateur, je peux réinitialiser mon mot de passe

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  Scenario: Demande de réinitialisation pour un compte inexistant
    Given I am on "/reset-password"
    When I fill in the following:
      | form[email] | not-existing-account@dt.dev |
    And I submit the "form" form
    Then the response status code should be 200
    Then I should see "Un email vous a été envoyé pour réinitialiser votre mot de passe"

  Scenario: Demande de réinitialisation pour un compte existant
    Given I am on "/reset-password"
    When I fill in the following:
      | form[email] | user@dt.dev |
    And I submit the "form" form
    Then the response status code should be 200
    And I should see "Un email vous a été envoyé pour réinitialiser votre mot de passe"
    And I should be on "/login"
    Then I check the last received email
    Then the subject should contain "Réinitialisation de votre mot de passe"
    When I follow the first link in the last received email
    Then the response status code should be 200
    When I fill in the following:
      | form[plainPassword][first]  | NotSoWeakPassword@1 |
      | form[plainPassword][second] | NotSoWeakPassword@1 |
    And I submit the "form" form
    Then the response status code should be 200
    Then I should see "Votre mot de passe a bien été mis à jour."
    And I should be on "/login"

  Scenario: Demande de réinitialisation pour un email avec la mauvaise case
    Given I am on "/reset-password"
    When I fill in the following:
      | form[email] | upper@dt.dev |
    And I submit the "form" form
    Then the response status code should be 200
    And I should see "Un email vous a été envoyé pour réinitialiser votre mot de passe"
    And I should be on "/login"
    Then I check the last received email
    Then the subject should contain "Réinitialisation de votre mot de passe"
