Feature: DAT-2 Authentification d'un utilisateur
  L'utilisateur est en mesure de se connecter

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  Scenario: Je suis redirigé vers le formulaire de login
    When I go to "/"
    Then I should be on "/login"

  Scenario: Si les identifiants sont incorrects j'ai une erreur explicite
    Given I am logged in as "no-account@dt.dev" with "no"
    Then I should see "Identifiants invalides."
    Given I am logged in as "user@dt.dev" with "user"
    Then I should be on "/flux"
    And the response status code should be 200
    And I should see "Vous devez renseigner toutes les informations obligatoires de votre profil et de votre société"

  Scenario: Je peux me connecter et me déconnecter si j'ai un compte
    Given I am logged in as "admin@dt.dev" with "admin"
    Then I should be on "/flux"
    And the response status code should be 200
    Then I follow "John DOE"
    When I follow "Déconnexion"
    Then I should be on "/login"

  Scenario: Je peux me connecter avec une adresse mail insensible à la casse
    Given I am logged in as "upper@dt.dev" with "upper"
    Then I should be on "/flux"
    And the response status code should be 200