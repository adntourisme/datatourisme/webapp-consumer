Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | output_type.yml |
      | application_type.yml |
      | user_type.yml |
      | user.yml |
      | flux.yml |
      | application.yml |
      | process.yml     |
      | download.yml |

  Scenario: Je peux voir les téléchargements d'un flux
    Given I am logged in as "certified@dt.dev" with "certified"
    Then the response status code should be 200
    Then I clean up the GET globals
    When I go to "/flux/2/downloads"
    Then the response status code should be 200
    And I should see "Téléchargement"
    And I should see "Méthode"
    And I should see "Application"

  Scenario: J'ai un message spécifique s'il n'y a pas de téléchargement
    Given I am logged in as "certified@dt.dev" with "certified"
    Then I should be on "/flux"
    And the response status code should be 200
    Then I follow "Nouveau flux"
    Then I fill in the following:
      |form[name] | Nouveau flux |
      |form[description] | Nouvelle description |
    And I submit the "form" form
    Then I go to "/flux"
    And I follow "Nouveau flux"
    Then I should see "Flux : Nouveau flux"
    Then I clean up the GET globals
    Then I follow "Téléchargement"
    Then I should see "Aucune version n'a encore été produite"

  Scenario: Je peux demander la génération du fichier de cache à beanstalk
    Given I am logged in as "certified@dt.dev" with "certified"
    Then I should be on "/flux"
    And the response status code should be 200
    When I go to "/flux/65/download"
    Then I submit the "form" form
    And the response status code should be 200
    When I go to "/flux/65/download/manual"
    And the response status code should be 200
    Then I wait for 15 seconds
    Then I clean up the GET globals
    Then I go to "/flux/65/processes"
    Then I should see "Télécharger la dernière version"
    Then I should see "En attente"
    When I go to "/internal/api/run/65"
    And the response status code should be 200
    Then I go to "/flux/65/editor"
    Then I fill in the following:
      | expert_editor[sparqlRequest]        | CONSTRUCT {?s ?p ?o2} WHERE {?s ?p ?o2} limit 10        |
    Then I submit the "FORM" form
    When I check the last received email
    Then the subject should contain "Les données du flux Fluxtest sont disponibles"

    Then I go to "/flux/65/processes"
    Then I clean up the GET globals
    And I should see "Programmé le"

    Then I go to "/flux/65/parameters"
    And I download the data file for flux "65"
    Then the response status code should be 200
    And the response content type should be "application/xml"
    Then I clean up the GET globals
    And I go to "/flux/download/65"
    Then I should see "Webservice"

#  Scenario: Si le fichier n'a pas été téléchargé depuis une semaine alors le flux est desactivé
#    Given I am logged in as "certified@dt.dev" with "certified"
#    Then I go to "/internal/flux/63"
#    Then the response status code should be 200
#    And the flux "63" should be unactive
#
#  Scenario: Si le flux est désactivé alors on peut le réactiver
#    Given I am logged in as "certified@dt.dev" with "certified"
#    Then I go to "/flux/download/63/manual"
#    And I submit the "form" form
#    Then the response status code should be 200
#    And the flux "63" should be active
#    Then I go to "/internal/flux/63"
#    Then the response status code should be 200
#    And the flux "63" should be active
#    Then I check the last received email
#    And the subject should contain "Les données du flux"
#
#  Scenario: Je peux télécharger le rapport de téléchargement en tant qu'administrateur
#    Given I am logged in as "admin@dt.dev" with "admin"
#    Then I should be on "/flux"
#    And the response status code should be 200
#    Then I follow "Télécharger un rapport de téléchargements"
#    Then I should see "Sélectionnez une période"
#    Then I submit the "form" form
#    Then the response status code should be 200
#
#  Scenario: J'ai une erreur 503 si je demande demande la première génération du fichier via le webservice
#    Given I download the data file for flux "64"
#    Then the response status code should be 503