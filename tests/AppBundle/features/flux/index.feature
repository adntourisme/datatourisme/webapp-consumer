Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | output_type.yml |
      | user_type.yml |
      | user.yml |

  Scenario: Un message est affiché s'il n'y a pas de flux
    Given I am logged in as "user@dt.dev" with "user"
    Then I should be on "/flux"
    And the response status code should be 200
    And I should see "Aucun flux ne correspond à votre recherche."

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | output_type.yml |
      | organization_type.yml |
      | organization.yml |
      | user.yml |
      | flux.yml |

  Scenario: Le message n'est pas affiché s'il y a des flux
    Given I am logged in as "user@dt.dev" with "user"
    Then I should be on "/flux"
    And the response status code should be 200
    And I should not see "Aucun flux ne correspond à votre recherche."
