Feature: Administration des utilisateurs

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  Scenario: En tant que non admin je ne peux pas accéder à l'interface d'administration
    Given I am logged in as "user@dt.dev" with "user"
    Then the response status code should be 200
    Then I clean up the GET globals
    When I go to "/admin/user"
    Then the response status code should be 403
    Then I disconnect

  Scenario: Certifier un compte
    Given I am logged in as "admin@dt.dev" with "admin"
    Then the response status code should be 200
    Then I clean up the GET globals
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I follow "Certifier"
    Then I should see "Certifier l'utilisateur"
    Then I submit the "form" form
    Then the response status code should be 200
    When I check the last received email
    Then the subject should contain "Votre compte a été certifié"
    Then I disconnect

    Given I am logged in as "user@dt.dev" with "user"
    Then the response status code should be 200
    Then I should be on "/flux"
    Then I clean up the GET globals
    Then I go to "/account/data-gouv"
    And the response status code should be 200

  Scenario: Décertifier un compte
    Given I am logged in as "admin@dt.dev" with "admin"
    Then the response status code should be 200
    Then I clean up the GET globals
    When I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I follow "Décertifier"
    Then I should see "Décertifier l'utilisateur"
    Then I submit the "form" form
    Then the response status code should be 200
    When I check the last received email
    Then the subject should contain "Votre compte a été décertifié"
    Then I disconnect

    Given I am logged in as "user@dt.dev" with "user"
    Then the response status code should be 200
    Then I clean up the GET globals
    Then I go to "/account/data-gouv"
    And the response status code should be 403

  Scenario: DAT-53 Bloquage et débloquage des utilisateurs
    Given I am logged in as "admin@dt.dev" with "admin"
    Then the response status code should be 200
    Then I clean up the GET globals
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I follow "Bloquer"
    Then I should see "Bloquer l'utilisateur"
    Then I submit the "form" form
    Then the response status code should be 200
    When I check the last received email
    Then the subject should contain "Votre compte a été bloqué"
    Then I disconnect

    Given I am logged in as "user@dt.dev" with "user"
    Then I should see "Le compte est bloqué"
    Given I am logged in as "admin@dt.dev" with "admin"
    Then I clean up the GET globals
    Then I go to "/admin/user?user_filter%5BfullName%5D=Jean"
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I follow "Débloquer"
    Then I should see "Débloquer l'utilisateur"
    Then I submit the "form" form
    Then the response status code should be 200
    When I check the last received email
    Then the subject should contain "Votre compte a été débloqué"

  Scenario: Pouvoir supprimer un utilisateur
    Given I am logged in as "admin@dt.dev" with "admin"
    Then the response status code should be 200
    Then I clean up the GET globals
    Then I go to "/admin/user"
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    Then I submit the "form" form
    Then I follow "DEAU Jean"
    Then I click the ".nav-tabs-custom > .nav-tabs >li:last-child a" element
    Then I click the ".btn.btn-danger" element
    Then I submit the "form" form
    Then the response status code should be 200
    Then I disconnect
    Given I am logged in as "user@dt.dev" with "user"
    Then I should see "Identifiants invalides"