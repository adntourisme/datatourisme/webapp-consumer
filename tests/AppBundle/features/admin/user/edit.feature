Feature:

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  Scenario: En tant qu'administrateur, je peux créer un éditer un utilisateur existant
    Given I am logged in as "admin@dt.dev" with "admin"
    Then the response status code should be 200
    Then I clean up the GET globals
    Then I go to "/admin/user"
    Then the response status code should be 200
    Then I fill in the following:
      | user_filter[fullName] | Jean |
    And I submit the "form" form
    Then I follow "DEAU Jean"
    Then the response status code should be 200
    When I fill in the following:
      | account[lastName] | Deaux |
    And I submit the "form" form
    And the response status code should be 200
    Then I should see "DEAUX Jean"