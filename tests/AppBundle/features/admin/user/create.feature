Feature: DAT-51 Création d'un compte

  Scenario: Je charge les fixtures.
    Given the database is empty
    And the following fixtures files are loaded:
      | user_type.yml |
      | user.yml |

  Scenario: En tant qu'administrateur, je peux créer un nouveau compte utilisateur et sa nouvelle structure
    Given I am logged in as "admin@dt.dev" with "admin"
    Then the response status code should be 200
    Then I clean up the GET globals
    When I go to "/admin/user/add"
    When I fill in the following:
      | account[lastName]     | DEAU           |
      | account[firstName]    | Jean           |
      | account[email]        | user@jean.deau |
    And I submit the "form" form
    Then I go to "/admin/user?user_filter%5BfullName%5D=Jean"
    Then I should see "DEAU Jean"
    Then I go to "/admin/user/add"
    When I fill in the following:
      | account[lastName]     | tourisme           |
      | account[firstName]    | DATA               |
      | account[email]        | user@jean.deau     |
    And I submit the "form" form
    Then I should see "Il existe déjà un utilisateur avec cette adresse mail"